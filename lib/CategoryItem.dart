import 'package:my/NewTaskItem.dart';
import 'package:flutter/material.dart';
import 'AppExpansionTile.dart';

import 'GlobalData.dart';

class CategoryItem extends StatefulWidget {
  final String proCategory;
  final String categopryName;
  final List<NewTaskItem> newTaskItems;

  CategoryItem({
    this.newTaskItems,
    this.proCategory,
    this.categopryName,
  });

  @override
  _CategoryItemState createState() => _CategoryItemState();
}

class _CategoryItemState extends State<CategoryItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: AppExpansionTile(
        title: Container(
          padding: EdgeInsets.only(right: 4),
          child: Row(
            children: <Widget>[
              Icon(
                Icons.arrow_right,
                color: colorTextSub,
              ),
              Text(
                widget.categopryName,
                style: TextStyle(
                    color: colorTextSub,
                    fontWeight: FontWeight.bold,
                    fontSize: textFontSizeSub),
              ),
            ],
          ),
        ),
        children: widget.newTaskItems,
      ),
    );
  }

  void onCardClicked(BuildContext context) {
    print('onCardClicked');
  }

  
}
