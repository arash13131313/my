import 'package:my/CategoryItem.dart';
import 'package:my/DashboardItem.dart';
import 'package:my/KPI/AllProcessPage.dart';
import 'package:my/KPI/AllUsersPage.dart';
import 'package:my/KPI/GaugesPage.dart';

import 'AppExpansionTile.dart';
import 'GlobalData.dart';
import 'NewTaskItem.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class DarwerPage extends StatefulWidget {
  final List<NewTaskItem> taskItems;
  final List<DashboardItem> dashboardItems;
  final Function callback;
  final int active;

  DarwerPage({
    Key key,
    this.taskItems,
    this.callback,
    this.dashboardItems,
    this.active,
  }) : super(key: key);

  @override
  _DarwerPageState createState() => _DarwerPageState();
}

class _DarwerPageState extends State<DarwerPage>
    with AutomaticKeepAliveClientMixin {
  final GlobalKey<AppExpansionTileState> expansionTileBox = GlobalKey();
  final GlobalKey<AppExpansionTileState> expansionTileNewTask = GlobalKey();
  final GlobalKey<AppExpansionTileState> expansionTileDashboard = GlobalKey();
  final GlobalKey<AppExpansionTileState> expansionTileKPI = GlobalKey();
  bool isActiveInbox;
  bool isActiveSent;
  bool isActiveDraft;
  bool isActiveStoped;
  bool isBoxExpanded;
  bool isNewTaskExpanded = false;
  bool isDashboardExpanded = false;
  bool isKPIExpanded = false;
  List<NewTaskItem> taskItems = List();
  bool isCategoriesSet = false;
  List<CategoryItem> categories = List();
  @override
  bool get wantKeepAlive => true;
  @override
  void initState() {
    if (widget.active == 0) {
      isActiveInbox = true;
    } else if (widget.active == 1) {
      isActiveSent = true;
    } else if (widget.active == 2) {
      isActiveDraft = true;
    } else if (widget.active == 3) {
      isActiveStoped = true;
    }
    isBoxExpanded = true;
    isNewTaskExpanded = true;
    isDashboardExpanded = true;
    isKPIExpanded = true;
    if (widget.taskItems == null) {
      taskItems.add(NewTaskItem(
        categoryName: 'بدون دسته بندی',
        proCategory: ' ',
        proTitle: ' ',
      ));
    } else {
      taskItems = widget.taskItems;
    }
    isCategoriesSet = false;
    //print('dash Item = ' + widget.dashboardItems[0].title);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: EdgeInsets.only(top: 30),
        child: Column(
          children: <Widget>[
            AppExpansionTile(
                initiallyExpanded: true,
                key: expansionTileBox,
                onExpansionChanged: (bool isOpened) {
                  setBoxExpandingState(isOpened);
                },
                title: Row(
                  children: <Widget>[
                    Icon(
                      isBoxExpanded
                          ? FontAwesomeIcons.boxOpen
                          : FontAwesomeIcons.box,
                      color: colorTextPrimary,
                      size: textFontSizeTitle,
                    ),
                    SizedBox(width: 12),
                    Text(
                      'صندوق',
                      style: TextStyle(
                          color: colorTextPrimary,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle),
                    ),
                  ],
                ),
                backgroundColor:
                    Theme.of(context).accentColor.withOpacity(0.025),
                children: <Widget>[
                  ListTile(
                    title: Row(
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.inbox,
                          color:
                              this.isActiveInbox == null || !this.isActiveInbox
                                  ? colorTextSub
                                  : colorAccent,
                          size: this.isActiveInbox == null || !isActiveInbox
                              ? textFontSizeSub
                              : textFontSizeSub + 2,
                        ),
                        SizedBox(width: 8),
                        Text(
                          'دریافت شده',
                          style: TextStyle(
                            color: this.isActiveInbox == null ||
                                    !this.isActiveInbox
                                ? colorTextSub
                                : colorAccent,
                            fontWeight: this.isActiveInbox == null ||
                                    !this.isActiveInbox
                                ? FontWeight.normal
                                : FontWeight.bold,
                            fontSize:
                                this.isActiveInbox == null || !isActiveInbox
                                    ? textFontSizeSub
                                    : textFontSizeSub + 2,
                          ),
                        ),
                      ],
                    ),
                    onTap: () {
                      setState(() {
                        this.isActiveInbox = true;
                        this.isActiveSent = false;
                        this.isActiveDraft = false;
                        this.isActiveStoped = false;
                        this.widget.callback(0);
                        Navigator.of(context).pop();
                      });
                    },
                  ),
                  ListTile(
                    title: Row(
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.forward,
                          color: this.isActiveSent == null || !this.isActiveSent
                              ? colorTextSub
                              : colorAccent,
                          size: this.isActiveSent == null || !isActiveSent
                              ? textFontSizeSub
                              : textFontSizeSub + 2,
                        ),
                        SizedBox(width: 8),
                        Text(
                          'ارسالی',
                          style: TextStyle(
                            color:
                                this.isActiveSent == null || !this.isActiveSent
                                    ? colorTextSub
                                    : colorAccent,
                            fontWeight:
                                this.isActiveSent == null || !this.isActiveSent
                                    ? FontWeight.normal
                                    : FontWeight.bold,
                            fontSize: this.isActiveSent == null || !isActiveSent
                                ? textFontSizeSub
                                : textFontSizeSub + 2,
                          ),
                        ),
                      ],
                    ),
                    onTap: () {
                      setState(() {
                        this.isActiveInbox = false;
                        this.isActiveSent = true;
                        this.isActiveDraft = false;
                        this.isActiveStoped = false;
                        this.widget.callback(1);
                        Navigator.of(context).pop();
                      });
                    },
                  ),
                  ListTile(
                    title: Row(
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.save,
                          color:
                              this.isActiveDraft == null || !this.isActiveDraft
                                  ? colorTextSub
                                  : colorAccent,
                          size: this.isActiveDraft == null || !isActiveDraft
                              ? textFontSizeSub
                              : textFontSizeSub + 2,
                        ),
                        SizedBox(width: 8),
                        Text(
                          'پیش نویس',
                          style: TextStyle(
                            color: this.isActiveDraft == null ||
                                    !this.isActiveDraft
                                ? colorTextSub
                                : colorAccent,
                            fontWeight: this.isActiveDraft == null ||
                                    !this.isActiveDraft
                                ? FontWeight.normal
                                : FontWeight.bold,
                            fontSize:
                                this.isActiveDraft == null || !isActiveDraft
                                    ? textFontSizeSub
                                    : textFontSizeSub + 2,
                          ),
                        ),
                      ],
                    ),
                    onTap: () {
                      setState(() {
                        this.isActiveInbox = false;
                        this.isActiveSent = false;
                        this.isActiveDraft = true;
                        this.isActiveStoped = false;
                        this.widget.callback(2);
                        Navigator.of(context).pop();
                      });
                    },
                  ),
                  ListTile(
                    title: Row(
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.stop,
                          color: this.isActiveStoped == null ||
                                  !this.isActiveStoped
                              ? colorTextSub
                              : colorAccent,
                          size: this.isActiveStoped == null || !isActiveStoped
                              ? textFontSizeSub
                              : textFontSizeSub + 2,
                        ),
                        SizedBox(width: 8),
                        Text(
                          'متوقف شده',
                          style: TextStyle(
                            color: this.isActiveStoped == null ||
                                    !this.isActiveStoped
                                ? colorTextSub
                                : colorAccent,
                            fontWeight: this.isActiveStoped == null ||
                                    !this.isActiveStoped
                                ? FontWeight.normal
                                : FontWeight.bold,
                            fontSize:
                                this.isActiveStoped == null || !isActiveStoped
                                    ? textFontSizeSub
                                    : textFontSizeSub + 2,
                          ),
                        ),
                      ],
                    ),
                    onTap: () {
                      setState(() {
                        this.isActiveInbox = false;
                        this.isActiveSent = false;
                        this.isActiveDraft = false;
                        this.isActiveStoped = true;
                        this.widget.callback(3);
                        Navigator.of(context).pop();
                      });
                    },
                  ),
                ]),
            AppExpansionTile(
              onExpansionChanged: (bool isOpened) {
                setNewTaskExpandingState(isOpened);
              },
              key: expansionTileNewTask,
              title: Row(
                children: <Widget>[
                  Icon(
                    FontAwesomeIcons.tasks,
                    color: colorTextPrimary,
                    size: textFontSizeTitle,
                  ),
                  SizedBox(width: 8),
                  Text(
                    'فرآیند جدید',
                    style: TextStyle(
                        color: colorTextPrimary,
                        fontWeight: FontWeight.bold,
                        fontSize: textFontSizeTitle),
                  ),
                ],
              ),
              backgroundColor: Theme.of(context).accentColor.withOpacity(0.025),
              children: getNewTaskListTiles(),
            ),
            AppExpansionTile(
              onExpansionChanged: (bool isOpened) {
                setDashboardExpandindgState(isOpened);
              },
              key: expansionTileDashboard,
              title: Row(
                children: <Widget>[
                  Icon(
                    FontAwesomeIcons.chartLine,
                    color: colorTextPrimary,
                    size: textFontSizeTitle,
                  ),
                  SizedBox(width: 8),
                  Text(
                    'داشبورد تحلیلی',
                    style: TextStyle(
                        color: colorTextPrimary,
                        fontWeight: FontWeight.bold,
                        fontSize: textFontSizeTitle),
                  ),
                ],
              ),
              backgroundColor: Theme.of(context).accentColor.withOpacity(0.025),
              children:
                  widget.dashboardItems == null ? [] : getDashboardListTiles(),
            ),
            AppExpansionTile(
              initiallyExpanded: false,
              key: expansionTileKPI,
              onExpansionChanged: (bool isOpened) {
                setKPIExpandindgState(isOpened);
              },
              title: Row(
                children: <Widget>[
                  Icon(
                    isKPIExpanded
                        ? FontAwesomeIcons.chartBar
                        : FontAwesomeIcons.chartPie,
                    color: colorTextPrimary,
                    size: textFontSizeTitle,
                  ),
                  SizedBox(width: 12),
                  Text(
                    'شاخص‌های کلیدی عملکرد',
                    style: TextStyle(
                        color: colorTextPrimary,
                        fontWeight: FontWeight.bold,
                        fontSize: textFontSizeTitle),
                  ),
                ],
              ),
              backgroundColor: Theme.of(context).accentColor.withOpacity(0.025),
              children: <Widget>[
                ListTile(
                  title: Row(
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.list,
                        color: colorTextPrimary,
                        size: 15,
                      ),
                      SizedBox(width: 8),
                      Text(
                        'فرآیندهای انتخابی',
                        style: TextStyle(
                          color: colorTextPrimary,
                          fontWeight: FontWeight.normal,
                          fontSize: textFontSizeSub + 1,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GaugesPage(
                                  id: 1,
                                  title: 'فرآیندهای انتخابی',
                                )));
                  },
                ),
                ListTile(
                  title: Row(
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.tasks,
                        color: colorTextPrimary,
                        size: 15,
                      ),
                      SizedBox(width: 8),
                      Text(
                        'وظایف انتخابی',
                        style: TextStyle(
                          color: colorTextPrimary,
                          fontWeight: FontWeight.normal,
                          fontSize: textFontSizeSub + 1,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GaugesPage(
                                  id: 2,
                                  title: 'وظایف انتخابی',
                                )));
                  },
                ),
                ListTile(
                  title: Row(
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.user,
                        color: colorTextPrimary,
                        size: 15,
                      ),
                      SizedBox(width: 8),
                      Text(
                        'کاربران انتخابی',
                        style: TextStyle(
                          color: colorTextPrimary,
                          fontWeight: FontWeight.normal,
                          fontSize: textFontSizeSub + 1,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => GaugesPage(
                                  id: 3,
                                  title: 'کاربران انتخابی',
                                )));
                  },
                ),
                ListTile(
                  title: Row(
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.borderAll,
                        color: colorTextPrimary,
                        size: 15,
                      ),
                      SizedBox(width: 8),
                      Text(
                        'کلیه فرآیندها',
                        style: TextStyle(
                          color: colorTextPrimary,
                          fontWeight: FontWeight.normal,
                          fontSize: textFontSizeSub + 1,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AllProcessPage()));
                  },
                ),
                ListTile(
                  title: Row(
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.users,
                        color: colorTextPrimary,
                        size: 15,
                      ),
                      SizedBox(width: 8),
                      Text(
                        'کلیه کاربران',
                        style: TextStyle(
                          color: colorTextPrimary,
                          fontWeight: FontWeight.normal,
                          fontSize: textFontSizeSub + 1,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => AllUsersPage()));
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> getNewTaskListTiles() {
    //print('taskItems lenght = ' + taskItems.length.toString());
    if (!isCategoriesSet) {
      _createCategiries();
    }
    //print('taskItems lenght = ' + taskItems.length.toString());

    List<Widget> item = <Widget>[
      ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemCount: categories.length,
        itemBuilder: (BuildContext context, int index) {
          Widget catItem = CategoryItem(
            categopryName: categories[index].categopryName == null
                ? ' '
                : categories[index].categopryName,
            proCategory: categories[index].proCategory == null
                ? ' '
                : categories[index].proCategory,
            newTaskItems: categories[index].newTaskItems == null
                ? ' '
                : categories[index].newTaskItems,
          );
          return catItem;
        },
      )
    ];

    return item;
  }

  List<Widget> getDashboardListTiles() {
    List<Widget> item = <Widget>[
      ListView.builder(
        shrinkWrap: true,
        itemCount: widget.dashboardItems.length,
        itemBuilder: (BuildContext context, int index) {
          Widget dashItem = DashboardItem(
            id: widget.dashboardItems[index].id ?? ' ',
            userId: widget.dashboardItems[index].userId ?? ' ',
            title: widget.dashboardItems[index].title ?? ' ',
            type: widget.dashboardItems[index].type ?? ' ',
            displayOrder: widget.dashboardItems[index].displayOrder ?? ' ',
            craetedAt: widget.dashboardItems[index].craetedAt ?? ' ',
            shareType: widget.dashboardItems[index].shareType ?? ' ',
            owenershipType: widget.dashboardItems[index].owenershipType ?? ' ',
            ownerId: widget.dashboardItems[index].ownerId ?? ' ',
          );
          return dashItem;
        },
      )
    ];

    return item;
  }

  setBoxExpandingState(bool isOpened) {
    setState(() {
      if (isOpened) {
        expansionTileNewTask.currentState.setExpanded(!isOpened);
        expansionTileDashboard.currentState.setExpanded(!isOpened);
        expansionTileKPI.currentState.setExpanded(!isOpened);
      }
      isBoxExpanded = isOpened;
    });
  }

  setNewTaskExpandingState(bool isOpened) {
    setState(() {
      if (isOpened) {
        expansionTileBox.currentState.setExpanded(!isOpened);
        expansionTileDashboard.currentState.setExpanded(!isOpened);
        expansionTileKPI.currentState.setExpanded(!isOpened);
      }
      isNewTaskExpanded = isOpened;
    });
  }

  setDashboardExpandindgState(bool isOpened) {
    setState(() {
      if (isOpened) {
        expansionTileBox.currentState.setExpanded(!isOpened);
        expansionTileNewTask.currentState.setExpanded(!isOpened);
        expansionTileKPI.currentState.setExpanded(!isOpened);
      }
      isDashboardExpanded = isOpened;
    });
  }

  setKPIExpandindgState(bool isOpened) {
    setState(() {
      if (isOpened) {
        expansionTileBox.currentState.setExpanded(!isOpened);
        expansionTileNewTask.currentState.setExpanded(!isOpened);
        expansionTileDashboard.currentState.setExpanded(!isOpened);
      }
      isKPIExpanded = isOpened;
    });
  }

  void _createCategiries() {
    List<NewTaskItem> tempTaskItems = List();
    tempTaskItems.addAll(taskItems);
    int n = tempTaskItems.length;
    print('tempTaskItems.length' + tempTaskItems.length.toString());
    int t = 0;
    while (tempTaskItems.length > 0) {
      categories.add(CategoryItem(
        categopryName: tempTaskItems[0].categoryName,
        proCategory: tempTaskItems[0].categoryName,
        newTaskItems: List(),
      ));
      print('Categories $t = ' + categories[t].categopryName);
      t++;
      n = tempTaskItems.length;
      for (var i = 0; i < n; i++) {
        if (tempTaskItems[i].categoryName ==
            categories[categories.length - 1].categopryName) {
          categories[categories.length - 1].newTaskItems.add(tempTaskItems[i]);
          tempTaskItems.removeAt(i);
          n = tempTaskItems.length;
          i--;
        }
      }
    }

    print('Categories : ' + categories.toString());
    isCategoriesSet = true;
  }
}
