import 'package:my/Dashboard/ChartInfo.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class BarChartHorizontal extends StatefulWidget {
  final ChartInfo chartsInfo;

  const BarChartHorizontal({this.chartsInfo});
  @override
  _BarChartHorizontalState createState() => _BarChartHorizontalState();
}

class _BarChartHorizontalState extends State<BarChartHorizontal> {
  List<charts.Series<BarChartInfo, String>> _createSampleData() {
    var measureTitles = widget.chartsInfo.measureTitle;
    var domainTitles = widget.chartsInfo.domainTitle;
    var measures = widget.chartsInfo.measures;
    List<BarChartInfo> barChartInfo = List();
    List<List<BarChartInfo>> barChartInfoList = List();
    for (var j = 0; j < domainTitles.length; j++) {
      for (var i = 0; i < measureTitles.length; i++) {
        print('measures.values.toList()[$j][$i] = ' +
            (measures.values.toList()[j][i] ?? ' its null '));
        barChartInfo.add(
          BarChartInfo(domainTitles[j],
              double.parse((measures.values.toList()[j][i] ?? '0.0')),
              color: getChartColor(i % 10)),
        );
      }
    }
    String cur = ' ';
    print('lenf]gh = ' + barChartInfo.length.toString());
    for (var i = 0; i < measureTitles.length; i++) {
      List<BarChartInfo> b = List();
      for (var j = 0; j < barChartInfo.length; j++) {
        if (barChartInfo[j].domain != cur) {
          b.add(barChartInfo[j]);
          cur = barChartInfo[j].domain;
          barChartInfo.removeAt(j);
          j--;
        }
      }
      barChartInfoList.add(b);
    }

    List<charts.Series<BarChartInfo, String>> series = List();

    for (var i = 0; i < barChartInfoList.length; i++) {
      series.add(
        charts.Series<BarChartInfo, String>(
          id: widget.chartsInfo.measureTitle[i],
          domainFn: (BarChartInfo gross, _) => gross.domain,
          measureFn: (BarChartInfo gross, _) => gross.measure,
          colorFn: (BarChartInfo gross, _) => gross.color,
          data: barChartInfoList[i],
        ),
      );
    }

    return series;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      padding: EdgeInsets.only(top: 8, bottom: 1, right: 5 ,left: 5),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12.0),
            bottomLeft: Radius.circular(12.0),
            bottomRight: Radius.circular(12.0),
            topRight: Radius.circular(12.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              offset: Offset(1.1, 1.1),
              blurRadius: 10.0),
        ],
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        reverse: true,
        child: Container(
          padding: EdgeInsets.all(8),
          width: 600,
          height: 350,
          //width: MediaQuery.of(context).size.width-50,
          child: charts.BarChart(
            _createSampleData(),
            animate: true,
            vertical: false,
            domainAxis: charts.OrdinalAxisSpec(
              viewport: new charts.OrdinalViewport(
                  widget.chartsInfo
                      .domainTitle[widget.chartsInfo.domainTitle.length - 1],
                  15),
              renderSpec: charts.SmallTickRendererSpec(
                // Tick and Label styling here.
                labelStyle: charts.TextStyleSpec(
                    color: charts.MaterialPalette.black, fontFamily: 'Sans'),
                // Change the line colors to match text color.
                lineStyle:
                    charts.LineStyleSpec(color: charts.MaterialPalette.black),
              ),
            ),
            barGroupingType: charts.BarGroupingType.grouped,
            behaviors: [
              //new charts.InitialHintBehavior(maxHintTranslate: 2.0),
              new charts.PanAndZoomBehavior(),
              new charts.ChartTitle(widget.chartsInfo.title,
                  behaviorPosition: charts.BehaviorPosition.top,
                  titleOutsideJustification:
                      charts.OutsideJustification.middleDrawArea,
                  innerPadding: 18),
              new charts.SeriesLegend(
                showMeasures: true,
                horizontalFirst: false,
                position: charts.BehaviorPosition.bottom,
                desiredMaxRows: generrateDesiredRows(
                    widget.chartsInfo.measureTitle.length),
                outsideJustification:
                    charts.OutsideJustification.middleDrawArea,
                entryTextStyle: charts.TextStyleSpec(
                    color: charts.Color(r: 0, g: 0, b: 0),
                    fontFamily: 'Sans',
                    fontSize: 11),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
