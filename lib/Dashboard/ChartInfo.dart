import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:charts_flutter/flutter.dart' as charts;

class ChartInfo {
  String type;
  String title;
  String scalarTitle;
  double scalarValue;
  String scalarIcon;
  double gaugeMax;
  double gaugeMin;
  double gaugeValue;
  List<dynamic> domainTitle;
  List<dynamic> measureTitle;
  Map<String,dynamic> measures;
  Map<String,dynamic> gridFields;
  List<dynamic> gridData;

  ChartInfo({
    this.type,
    this.domainTitle,
    this.measureTitle,
    this.title,
    this.measures,
    this.gridData,
    this.gridFields,
    this.gaugeMax,
    this.gaugeMin,
    this.gaugeValue,
    this.scalarTitle,
    this.scalarIcon,
    this.scalarValue,
  });

  static Map<String, dynamic> getSampleJson() {
    String body = '[ ' +
        '{' +
        '"title": "Gozaresh1",' +
        '"type": "bar",' +
        '"domain": [' +
        '"98-8",' +
        '"98-9",' +
        '"98-10"' +
        '],' +
        '"measure": [' +
        '[' +
        '40,' +
        '35,' +
        '45' +
        '],' +
        '[' +
        '35,' +
        '40,' +
        '30' +
        '],' +
        '[' +
        '42,' +
        '38,' +
        '40' +
        ']' +
        '],' +
        '"measure-title": [' +
        '"مجموع حضور",' +
        '"مجموع تخمین",' +
        '"میزان رمان کارکرد"' +
        ']' +
        '}' +
        ']';
    Map<String, dynamic> json = jsonDecode(body);
    return json;
  }
}

class BarChartInfo {
  final String domain;
  final double measure;
  final charts.Color color;

  BarChartInfo(this.domain, this.measure, {Color color})
      : this.color = new charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}

class PieChartInfo {
  final String domain;
  final double measure;
  final charts.Color color;

  PieChartInfo(this.domain, this.measure, {Color color})
      : this.color = new charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}

class LineChartInfo {
  final String domain;
  final double measure;
  final charts.Color color;

  LineChartInfo(this.domain, this.measure, {Color color})
      : this.color = new charts.Color(
            r: color.red, g: color.green, b: color.blue, a: color.alpha);
}

Color getChartColor(int i) {
  switch (i) {
    case 0:
      return Colors.deepOrange;
    case 1:
      return Colors.orange;
    case 2:
      return Colors.amber;
    case 3:
      return Colors.lime;
    case 4:
      return Colors.cyan;
    case 5:
      return Colors.blue;
    case 6:
      return Colors.purple;
    case 7:
      return Colors.pink;
    case 8:
      return Colors.orange; 
    case 9:
      return Colors.yellow;
    case 10:
      return Colors.green;
    case 11:
      return Colors.cyan;
    case 12:
      return Colors.blue;
    default:
      return Colors.blue;
  }
}
int generrateDesiredRows(int measureCount) {
    int desiredRows = 1;
    if (measureCount > 9) {
      desiredRows = 4;
    } else if (measureCount > 6) {
      desiredRows = 3;
    } else if (measureCount > 3) {
      desiredRows = 2;
    }
    return desiredRows;
  }
