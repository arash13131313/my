import 'dart:convert';

import 'package:my/Dashboard/BarChartHorizontal.dart';
import 'package:my/Dashboard/ChartInfo.dart';
import 'package:my/Dashboard/ScalarChart.dart';
import 'package:flutter/material.dart';
import 'package:my/utils/AnimationHandler.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../GlobalData.dart';
import 'BarChart.dart';
import 'DataGrid.dart';
import 'GaugeChart.dart';
import 'LineChart.dart';
import 'PieChart.dart';
import 'package:http/http.dart' as http;
import 'package:progress_dialog/progress_dialog.dart';

class Dashboard extends StatefulWidget {
  final String dashboardId;
  final String dashboardTitle;

  const Dashboard({Key key, this.dashboardId, this.dashboardTitle})
      : super(key: key);
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  List<ChartInfo> charts = List();
  ProgressDialog pr;
  int animCounter = 1;

  bool isLoaded = false;
  @override
  void initState() {
    super.initState();

    getAccessToken().then((accessToken) {
      _getCharts(accessToken).then((ch) {
        setState(() {
          charts = ch;
          isLoaded = true;
          pr.dismiss();
        });
      });
    });
  }

  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }

  Future<List<ChartInfo>> _getCharts(accessToken) async {
    pr = new ProgressDialog(context,
        type: ProgressDialogType.Normal, isDismissible: true, showLogs: false);
    pr.style(
        message: 'در حال بارگذاری ...',
        borderRadius: 20.0,
        backgroundColor: Colors.white,
        progressWidget: Container(
          child: CircularProgressIndicator(),
          padding: EdgeInsets.all(12),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeOutBack,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: colorTextPrimary,
            fontSize: textFontSizeSub,
            fontWeight: FontWeight.w400),
        messageTextStyle: TextStyle(
            color: colorTextPrimary,
            fontSize: textFontSizeTitle,
            fontWeight: FontWeight.bold));
    pr.show();
    List<ChartInfo> charts = List();
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };
    String url =
        apiServer + 'mobiledashboard/dashlet-list/' + widget.dashboardId;
    print('url = ' + url);

    http.Response response = http.Response('{}', 200);
    response = await http.get(url, headers: requestHeaders);
    //print('respons body = ' + response.body);
    try {} catch (e) {
      print('failed' + e.toString());
      return null;
    }
    //print('response.body = ' + response.body);
    List<dynamic> json = jsonDecode(response.body);
    for (var i = 0; i < json.length; i++) {
      //print('json[i]["domain-title"] = ' + json[i]['domain-title'][0]);
      ChartInfo chart;
      //print('json[i][title]123123 = ${json[i]['title']}');
      if (json[i]['type'] == 'grid') {
        chart = ChartInfo(
          title: json[i]['title'],
          type: 'grid',
          domainTitle: json[i]['domain-title'],
          measureTitle: json[i]['measure-title'],
          measures: json[i]['measure'],
          gridFields: json[i]['fields'],
          gridData: json[i]['data'],
        );
      } else if (json[i]['type'] == 'gauge') {
        chart = ChartInfo(
          title: json[i]['title'],
          type: 'gauge',
          gaugeMin: json[i]['min'] * 1.0,
          gaugeMax: double.parse(json[i]['max'] ?? '40'),
          gaugeValue: double.parse(json[i]['CurrentValue'] ?? '0'),
        );
      } else if (json[i]['type'] == 'scalar') {
        chart = ChartInfo(
          title: json[i]['title'],
          type: 'scalar',
          scalarIcon: json[i]['icon'],
          scalarTitle: json[i]['second_title'],
          scalarValue: double.parse(json[i]['value'] ?? '0'),
        );
      } else {
        Map<String, dynamic> measure;
        try {
          measure = json[i]['measure'];
          print('measure OK for ${json[i]['title']}');
        } catch (e) {
          print('measure NOT OK for ${json[i]['title']}');
          measure = Map();
        }
        print('title = ${json[i]['title']}');
        chart = ChartInfo(
          title: json[i]['title'],
          type: json[i]['type'],
          domainTitle: json[i]['domain-title'],
          measureTitle: json[i]['measure-title'],
          measures: measure,
        );
      }
      //print('title' + chart.title);
      //print('chart measure = ' + chart.measures.values.toList()[0].toString());

      charts.add(chart);

      //print('measures ' + chart.measures[0][1].toString());
    }
    return charts;
  }

  Curve myCurve = Curves.easeOut;
  double myDelay = 0;

  List<Widget> _getChartsWidget() {
    List<Widget> chartList = List();
    List<Widget> scallarList = List();
    bool _addedWrapToCharts = false;
    for (var i = 0; i < charts.length; i++) {
      if (charts[i].type == 'column') {
        if (animCounter % 2 == 0) {
          chartList.add(
              BarChart(chartsInfo: charts[i]));
        } else {
          chartList.add(BarChart(chartsInfo: charts[i]));
        }
        animCounter++;
      } else if (charts[i].type == 'pie') {
        if (animCounter % 2 == 0) {
          chartList.add(
              PieChart(chartsInfo: charts[i]));
        } else {
          chartList.add(
              PieChart(chartsInfo: charts[i]));
        }
        animCounter++;
      } else if (charts[i].type == 'line') {
        if (animCounter % 2 == 0) {
          chartList.add(
              LineChart(chartsInfo: charts[i]));
        } else {
          chartList.add(
              LineChart(chartsInfo: charts[i]));
        }
        animCounter++;
      } else if (charts[i].type == 'grid') {
        if (animCounter % 2 == 0) {
          chartList.add(
              DataGrid(chartsInfo: charts[i]));
        } else {
          chartList.add(
              DataGrid(chartsInfo: charts[i]));
        }
        animCounter++;
      } else if (charts[i].type == 'bar') {
        if (animCounter % 2 == 0) {
          chartList.add(
              BarChartHorizontal(chartsInfo: charts[i]));
        } else {
          chartList.add(
              BarChartHorizontal(chartsInfo: charts[i]));
        }
        animCounter++;
      } else if (charts[i].type == 'gauge') {
        if (animCounter % 2 == 0) {
          chartList.add(
              GaugeChart(chartsInfo: charts[i]));
        } else {
          chartList.add(
              GaugeChart(chartsInfo: charts[i]));
        }
        animCounter++;
      } else if (charts[i].type == 'scalar') {
        if (!_addedWrapToCharts) {
          chartList.add(Wrap(
            children: scallarList,
          ));
          _addedWrapToCharts = true;
        }

        if (animCounter % 2 == 0) {
          scallarList.add(
              ScalarChart(chartsInfo: charts[i]));
        } else {
          scallarList.add(
              ScalarChart(chartsInfo: charts[i]));
        }
        animCounter++;
      }
    }

    return chartList;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF2F3F8),
      appBar: AppBar(
        backgroundColor: colorAccent,
        title: Hero(
          tag: 'dashboardTitle' + widget.dashboardId,
          child: Text(widget.dashboardTitle),
        ),
        //centerTitle: true,
      ),
      body: isLoaded
          ? Container(
              child: ListView(
                children: _getChartsWidget(),
              ),
            )
          : Container(),
    );
  }
}
