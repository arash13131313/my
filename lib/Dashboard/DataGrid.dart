import 'dart:async';

import 'package:my/Dashboard/ChartInfo.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'PaginatedDataTable.dart' as myPaginatedDataTable;

class Result {
  List<DataCell> cells;
  List<DataColumn> columns;
  List<String> cellTitle;

  Result({this.cells, this.columns, this.cellTitle});

  bool selected = false;
}

class ResultsDataSource extends DataTableSource {
  final List<Result> _results;
  ResultsDataSource(this._results);

  void _sort<T>(Comparable<T> getField(Result d), bool ascending) {
    _results.sort((Result a, Result b) {
      if (!ascending) {
        final Result c = a;
        a = b;
        b = c;
      }
      final Comparable<T> aValue = getField(a);
      final Comparable<T> bValue = getField(b);
      return Comparable.compare(aValue, bValue);
    });
    notifyListeners();
  }

  int _selectedCount = 0;

  @override
  DataRow getRow(int index) {
    assert(index >= 0);
    if (index >= _results.length) return null;
    final Result result = _results[index];
    return DataRow.byIndex(
        index: index, selected: result.selected, cells: result.cells);
  }

  @override
  int get rowCount => _results.length;

  @override
  bool get isRowCountApproximate => false;

  @override
  int get selectedRowCount => _selectedCount;

  void _selectAll(bool checked) {
    for (Result result in _results) result.selected = checked;
    _selectedCount = checked ? _results.length : 0;
    notifyListeners();
  }
}

class DataGrid extends StatefulWidget {
  final ChartInfo chartsInfo;

  DataGrid({Key key, this.chartsInfo}) : super(key: key);

  @override
  _DataGridState createState() => _DataGridState();
}

class _DataGridState extends State<DataGrid> {
  ResultsDataSource _resultsDataSource = ResultsDataSource([]);
  bool isLoaded = false;
  int _rowsPerPage = 5;
  int _sortColumnIndex;
  bool _sortAscending = true;

  void _sort<T>(
      Comparable<T> getField(Result d), int columnIndex, bool ascending) {
    _resultsDataSource._sort<T>(getField, ascending);
    setState(() {
      _sortColumnIndex = columnIndex;
      _sortAscending = ascending;
    });
  }

  Future<void> getData() async {
    List<Result> results = List();
    for (var i = 0; i < dataRows.length; i++) {
      Result r = Result(
        cells: dataRows[i].cells,
        cellTitle: cellNames[i],
      );
      results.add(r);
    }
    if (!isLoaded) {
      setState(() {
        _resultsDataSource = ResultsDataSource(results);
        isLoaded = true;
      });
    }
  }

  List<DataColumn> dataColumns = List();
  List<DataRow> dataRows = List();
  List<List<String>> cellNames = List();
  void _setDataColumnsRows() {
    //print('widget.chartsInfo.gridData = ' + widget.chartsInfo.gridData[0].values.toList()[0].toString());
    for (var i = 0; i < widget.chartsInfo.gridFields.length; i++) {
      dataColumns.add(
        DataColumn(
          label: Container(
              alignment: Alignment.center,
              child: Text(widget.chartsInfo.gridFields.keys.toList()[i])),
          //numeric: false,
          onSort: (int columnIndex, bool ascending) {
            _sort<String>((Result d) {
              print('Sorting String test $i = ' +
                  d.cellTitle[i]);
              return d.cellTitle[i].toString();
            }, columnIndex, ascending);
          },
        ),
      );
    }
    //print('DataCoumns length = ' + dataColumns.length.toString());

    for (var i = 0; i < widget.chartsInfo.gridData.length; i++) {
      List<DataCell> cells = List();
      List<String> cellName = List();
      //print('DataRow[ ');
      for (var j = 0; j < widget.chartsInfo.gridData[i].length; j++) {
        //print('Cell $j = ' + widget.chartsInfo.gridData[i].values.toList()[j].toString());
        cellName.add(widget.chartsInfo.gridData[i].values.toList()[j] ?? '-');
        cells.add(DataCell(
          Container(
            constraints: BoxConstraints(maxWidth: 160),
            alignment: Alignment.center,
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              reverse: true,
              primary: true,
              child: Container(
                  //constraints: BoxConstraints(maxWidth: 160),
                  alignment: Alignment.center,
                  child: Text(
                    widget.chartsInfo.gridData[i].values.toList()[j] ?? '-',
                    maxLines: 1,
                    textDirection: TextDirection.rtl,
                  )),
            ),
          ),
        ));
      }
      //print('cells length = ' + cells.length.toString());
      dataRows.add(DataRow(cells: cells));
      cellNames.add(cellName);
      //print(' ]');
    }
  }

  @override
  void initState() {
    super.initState();
    _setDataColumnsRows();
    getData();
  }

  @override
  Widget build(BuildContext context) {
    //print('grid data length ${widget.chartsInfo.title} = ${widget.chartsInfo.gridData.length}');
    return widget.chartsInfo.gridData.length > 0
        ? SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Container(
              padding: EdgeInsets.all(2),
              width: MediaQuery.of(context).size.width,
              child: myPaginatedDataTable.PaginatedDataTable(
                  header: Text(widget.chartsInfo.title),
                  rowsPerPage: _rowsPerPage,
                  availableRowsPerPage: [5, 10, 15,20,25],
                  columnSpacing: 15,
                  onRowsPerPageChanged: (int value) {
                    setState(() {
                      _rowsPerPage = value;
                    });
                  },
                  sortColumnIndex: _sortColumnIndex,
                  sortAscending: _sortAscending,
                  onSelectAll: _resultsDataSource._selectAll,
                  columns: dataColumns,
                  source: _resultsDataSource),
            ),
          )
        : Container();
  }

  
}
