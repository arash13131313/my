import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

import '../GlobalData.dart';
import 'ChartInfo.dart';

class GaugeChart extends StatefulWidget {
  final ChartInfo chartsInfo;

  const GaugeChart({this.chartsInfo});

  @override
  _GaugeChartState createState() => _GaugeChartState();
}

class _GaugeChartState extends State<GaugeChart> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      padding: EdgeInsets.only(top: 8, bottom: 1),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12.0),
            bottomLeft: Radius.circular(12.0),
            bottomRight: Radius.circular(12.0),
            topRight: Radius.circular(12.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              offset: Offset(1.1, 1.1),
              blurRadius: 10.0),
        ],
      ),
      child: Container(
        padding: EdgeInsets.all(8),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                widget.chartsInfo.title,
                style: TextStyle(
                    fontWeight: FontWeight.bold, fontSize: textFontSizeTitle),
              ),
            ),
            Container(
              constraints: BoxConstraints(
                  maxWidth: MediaQuery.of(context).size.width,
                  maxHeight: 300),
              child: SfRadialGauge(
                axes: <RadialAxis>[
                  RadialAxis(
                    minimum: widget.chartsInfo.gaugeMin ?? 0.0,
                    maximum: widget.chartsInfo.gaugeMax ?? 100.0,
                    radiusFactor: 1,
                    ranges: <GaugeRange>[
                      GaugeRange(
                          startValue: 0,
                          endValue: widget.chartsInfo.gaugeMax / 4,
                          color: Colors.green),
                      GaugeRange(
                          startValue: widget.chartsInfo.gaugeMax / 4,
                          endValue: widget.chartsInfo.gaugeMax / 2,
                          color: Colors.amber),
                      GaugeRange(
                          startValue: widget.chartsInfo.gaugeMax / 2,
                          endValue: widget.chartsInfo.gaugeMax * (3 / 4),
                          color: Colors.orange),
                      GaugeRange(
                          startValue: widget.chartsInfo.gaugeMax * (3 / 4),
                          endValue: widget.chartsInfo.gaugeMax,
                          color: Colors.red),
                    ],
                    majorTickStyle: MajorTickStyle(
                        length: 0.08,
                        lengthUnit: GaugeSizeUnit.factor,
                        thickness: 4),
                    axisLabelStyle: GaugeTextStyle(
                        fontFamily: 'Sans',
                        fontSize: 12,
                        fontWeight: FontWeight.w800,
                        fontStyle: FontStyle.italic),
                    minorTickStyle: MinorTickStyle(
                      length: 0.04,
                      thickness: 2,
                      lengthUnit: GaugeSizeUnit.factor,
                      color: const Color(0xFFC4C4C4),
                    ),
                    axisLineStyle: AxisLineStyle(
                        color: const Color(0xFFDADADA),
                        thicknessUnit: GaugeSizeUnit.factor,
                        thickness: 0.1),
                    pointers: <GaugePointer>[
                      NeedlePointer(
                        value: widget.chartsInfo.gaugeValue ?? 30.0,
                        needleStartWidth: 2,
                        needleEndWidth: 5,
                        needleColor: const Color(0xFFF67280),
                        needleLength: 0.7,
                        lengthUnit: GaugeSizeUnit.factor,
                        enableAnimation: true,
                        animationType: AnimationType.bounceOut,
                        animationDuration: 1500,
                        knobStyle: KnobStyle(
                          knobRadius: 8,
                          sizeUnit: GaugeSizeUnit.logicalPixel,
                          color: const Color(0xFFF67280),
                        ),
                      ),
                    ],
                    annotations: <GaugeAnnotation>[
                      GaugeAnnotation(
                          widget: Container(
                            child: Text(
                              widget.chartsInfo.gaugeValue.toStringAsFixed(2),
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.bold),
                            ),
                          ),
                          angle: 90,
                          positionFactor: 0.5)
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SalesData {
  SalesData(this.year, this.sales);

  final String year;
  final double sales;
}
