import 'package:my/Dashboard/ChartInfo.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class PieChart extends StatefulWidget {
  final ChartInfo chartsInfo;

  const PieChart({this.chartsInfo});
  @override
  _PieChartState createState() => _PieChartState();
}

class _PieChartState extends State<PieChart> {
  List<charts.Series<PieChartInfo, String>> _createSampleData() {
    var domainTitles = widget.chartsInfo.domainTitle;
    var measures = widget.chartsInfo.measures;

    List<List<PieChartInfo>> barChartInfoList = List();
    for (var j = 0; j < domainTitles.length; j++) {
      List<PieChartInfo> barChartInfo = List();
      print('measures.keys.toList().length = ' +
          measures.keys.toList().length.toString());
      for (var i = 0; i < measures.keys.toList().length; i++) {
        //print('i = ' + i.toString() + '| j = ' + j.toString());

        barChartInfo.add(
          PieChartInfo(
              measures.keys.toList()[i][j] + i.toString(),
              double.parse(
                  double.parse((measures.values.toList()[i][j] ?? '0.0'))
                      .toStringAsFixed(2)),
              color: getChartColor(i % 10)),
        );
        //print('barChartInfo[$i] Measure = ' + barChartInfo[i].measure.toString());
      }
      barChartInfoList.add(barChartInfo);
    }
    List<charts.Series<PieChartInfo, String>> series = List();

    for (var i = 0; i < barChartInfoList.length; i++) {
      series.add(
        charts.Series<PieChartInfo, String>(
          id: ' ',
          domainFn: (PieChartInfo gross, _) => gross.domain,
          measureFn: (PieChartInfo gross, _) => gross.measure,
          colorFn: (PieChartInfo gross, _) => gross.color,
          data: barChartInfoList[i],
          labelAccessorFn: (PieChartInfo row, _) => '${row.measure}',
        ),
      );
    }

    return series;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      padding: EdgeInsets.only(top: 8, bottom: 1),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(12.0),
            bottomLeft: Radius.circular(12.0),
            bottomRight: Radius.circular(12.0),
            topRight: Radius.circular(12.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              offset: Offset(1.1, 1.1),
              blurRadius: 10.0),
        ],
      ),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Row(
          children: <Widget>[
            Container(
              constraints: BoxConstraints(maxHeight: 350, maxWidth: 450),
              //height: 350,
              //width: MediaQuery.of(context).size.width + 750,
              child: charts.PieChart(
                _createSampleData(),
                animate: true,
                defaultRenderer: charts.ArcRendererConfig(
                    arcRendererDecorators: [
                      charts.ArcLabelDecorator(
                          labelPosition: charts.ArcLabelPosition.auto)
                    ]),
                defaultInteractions: true,
                behaviors: [
                  charts.ChartTitle(widget.chartsInfo.title,
                      behaviorPosition: charts.BehaviorPosition.top,
                      titleOutsideJustification:
                          charts.OutsideJustification.middleDrawArea,
                      innerPadding: 18),
                ],
              ),
            ),
            Card(
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 4, vertical: 4),
                constraints: BoxConstraints(maxHeight: 330, maxWidth: 250),
                child: SingleChildScrollView(
                  child: Column(
                    children: _getPieLegends(),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> _getPieLegends() {
    List<Widget> pieLegends = List();
    var measures = widget.chartsInfo.measures;
    for (var i = 0; i < measures.keys.toList().length; i++) {
      pieLegends.add(Container(
        padding: EdgeInsets.only(right: 8, left: 4),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(double.parse((measures.values.toList()[i][0] ?? '0.0'))
                    .toStringAsFixed(2)),
                Expanded(
                  child: Container(),
                ),
                Container(
                  padding: EdgeInsets.only(right: 4),
                  alignment: Alignment.centerRight,
                  width: 150,
                  child: Text(
                    measures.keys.toList()[i],
                    softWrap: true,
                    textDirection: TextDirection.rtl,
                  ),
                ),
                Icon(
                  Icons.donut_small,
                  color: getChartColor(i % 10),
                ),
              ],
            ),
            Divider(),
          ],
        ),
      ));
    }
    return pieLegends;
  }
}
