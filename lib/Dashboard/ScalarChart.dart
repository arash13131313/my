import 'package:my/Dashboard/ChartInfo.dart';
import 'package:flutter/material.dart';
import 'package:icons_helper/icons_helper.dart';

import '../GlobalData.dart';

class ScalarChart extends StatelessWidget {
  final ChartInfo chartsInfo;

  const ScalarChart({Key key, this.chartsInfo}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    String iconName;
    double d = 3.3;
    TextStyle style =
        TextStyle(color: colorTextWhite, fontSize: textFontSizeTitle + 1);
    //print('icon = ' + chartsInfo.scalarIcon);
    iconName = chartsInfo.scalarIcon.substring(3, chartsInfo.scalarIcon.length);
    //print('iconName = ' + iconName);
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 0),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
        padding: EdgeInsets.only(top: 0, bottom: 0),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0),
              bottomLeft: Radius.circular(6.0),
              bottomRight: Radius.circular(6.0),
              topRight: Radius.circular(6.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                offset: Offset(1.1, 1.1),
                blurRadius: 10.0),
          ],
        ),
        child: Container(
          width: MediaQuery.of(context).size.width / d,
          height: MediaQuery.of(context).size.width / d,
          padding: EdgeInsets.all(0),
          child: Stack(
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(32.0),
                      bottomLeft: Radius.circular(6.0),
                      bottomRight: Radius.circular(6.0),
                      topRight: Radius.circular(6.0)),
                  color: Colors.lightBlue.shade200,
                ),
                width: MediaQuery.of(context).size.width / d,
                height: MediaQuery.of(context).size.width / d,
                child: Icon(
                  getIconGuessFavorFA(
                      name: iconName == 'money' ? 'moneyBill' : iconName),
                  size: MediaQuery.of(context).size.width / d / 1.5,
                  color: Colors.white,
                ),
              ),
              Container(
                padding: EdgeInsets.all(8),
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width / 3,
                height: MediaQuery.of(context).size.width / 3,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(32.0),
                      bottomLeft: Radius.circular(6.0),
                      bottomRight: Radius.circular(6.0),
                      topRight: Radius.circular(6.0)),
                  color: Colors.blue.withOpacity(0.7),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(chartsInfo.title, style: style),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(chartsInfo.scalarValue.toStringAsFixed(2),
                          style: style),
                    ),
                    Text(chartsInfo.scalarTitle, style: style),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
