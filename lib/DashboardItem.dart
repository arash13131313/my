import 'package:flutter/material.dart';
import 'package:my/GlobalData.dart';

import 'Dashboard/Dashboard.dart';

class DashboardItem extends StatefulWidget {
  final String id;
  final String userId;
  final String title;
  final String type;
  final String displayOrder;
  final String craetedAt;
  final String shareType;
  final String owenershipType;
  final String ownerId;

  DashboardItem({
    this.id,
    this.userId,
    this.title,
    this.type,
    this.displayOrder,
    this.craetedAt,
    this.shareType,
    this.owenershipType,
    this.ownerId,
  });
  factory DashboardItem.fromJson(Map<String, dynamic> parsedJson) {
    return DashboardItem(
      id: parsedJson['id'],
      userId: parsedJson['user_id'],
      title: parsedJson['title'],
      type: parsedJson['type'],
      displayOrder: parsedJson['displayOrder'],
      craetedAt: parsedJson['CreatedAt'],
      shareType: parsedJson['share_type'],
      owenershipType: parsedJson['ownership_type'],
      ownerId: parsedJson['owner_id'],
    );
  }
  @override
  _DashboardItemState createState() => _DashboardItemState();
}

class _DashboardItemState extends State<DashboardItem> {
  @override
  Widget build(BuildContext context) {
    var container = Container(
      padding: EdgeInsets.only(right: 8, top: 8, bottom: 8),
      child: Row(
        children: <Widget>[
          Icon(
            Icons.arrow_right,
            color: colorAccent,
          ),
          Hero(
            tag: 'dashboardTitle' +widget.id,
            child: Text(
              widget.title,
              style: TextStyle(
                  color: colorAccent,
                  fontWeight: FontWeight.normal,
                  fontSize: textFontSizeTitle -1),
            ),
          ),
        ],
      ),
    );
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => Directionality(
                    textDirection: TextDirection.ltr,
                    child: new Builder(
                      builder: (BuildContext context) {
                        return new MediaQuery(
                          data: MediaQuery.of(context).copyWith(
                            textScaleFactor: 1.0,
                          ),
                          child: Dashboard(
                            dashboardId: widget.id,
                            dashboardTitle: widget.title,
                          ),
                        );
                      },
                    ),
                  )),
        );
      },
      child: container,
    );
  }
}
