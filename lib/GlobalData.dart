import 'package:flutter/material.dart';
import 'package:shamsi_date/shamsi_date.dart';

final String apiServer = 'http://ls.arian.co.ir:8081/api/1.0/arian/';
final Color colorPrimary = Colors.deepOrange;
final Color colorAccent = Colors.deepOrangeAccent;
final Color colorSecond = Colors.orange;
final Color colorFlatButton = Colors.blue;
final Color colorTextWhite = Colors.white;
final Color colorTextPrimary = Colors.black87;
final Color colorTextTitle = Colors.black;
final Color colorTextSub = Colors.black54;
final Color colorTextSub2 = Colors.black38;
final Color colorDivider = Colors.grey[200];
final Color colorCorrect = Colors.green;
final Color colorWrong = Colors.red;
final Color colorDeactivated = Colors.grey;
final Color colorLineChart = Colors.blue;
final Color colorBackground = Color(0xFFF2F3F8);
final Color colorBubbles = Colors.amber;
final double textFontSizeTitle = 17;
final double textFontSizeSub = 13;
final double textFontSizeSplash = 17;
final List<Color> splashGradient = [
              Colors.deepOrange[300],
              Colors.deepOrange[400],
              Colors.deepOrange[500],
            ];

final String iframeServer =
    'http://ls.arian.co.ir:8081/sysarian/fa/modern/';
String convertToJalali(String date) {
  try {
    var datetime = DateTime.parse(date);
    var shamsy = Jalali.fromDateTime(datetime);
    String s = format2(shamsy);
    return '${datetime.hour}:${datetime.minute}:${datetime.second}' + ' ' + s;
  } catch (e) {
    return date;
  }
}

String format2(Date d) {
  final f = d.formatter;

  return '${f.yyyy}/${f.mm}/${f.dd}';
}
