import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:my/Dashboard/ChartInfo.dart';
import 'package:my/utils/AnimationHandler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../GlobalData.dart';
import 'FilterDialog.dart';
import 'KPIBarChart.dart';
import 'KPILineChart.dart';
import 'KPIScalarChart.dart';

class AllProcessPage extends StatefulWidget {
  @override
  _AllProcessPageState createState() => _AllProcessPageState();
}

class _AllProcessPageState extends State<AllProcessPage> {
  List<KPIScalarChart> _kpiScalarCharts = List();
  KPILineChart _lineChart = KPILineChart();
  KPIBarChart _barChart = KPIBarChart();
  bool _isScalarChartsLoaded = false;
  bool _isBarLineChartsLoaded = false;
  String unit, datetimeFix, appStatus, calculationCriterion, beginDate, endDate;
  int kpiTypeId;
  @override
  void initState() {
    super.initState();

    _lineChart = KPILineChart();
    _barChart = KPIBarChart();
    _kpiScalarCharts = List();
    _isScalarChartsLoaded = false;
    _isBarLineChartsLoaded = false;
    unit = "day";
    datetimeFix = "1 month";
    appStatus = "COMPLETED";
    calculationCriterion = "Min";
    beginDate = " ";
    endDate = " ";

    getAccessToken().then((accessToken) {
      _postGetScalarCharts(accessToken, true).then((list) {
        setState(() {
          _kpiScalarCharts = list;
          _isScalarChartsLoaded = true;
        });
      });
      _getBarLineCharts(accessToken, true).then((isLoaded) {
        setState(() {
          _isBarLineChartsLoaded = isLoaded;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorBackground,
      appBar: AppBar(
        title: Text('کلیه فرآیندها'),
        backgroundColor: colorAccent,
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              _showFilterDialog();
            },
            child: Row(
              children: <Widget>[
                Icon(Icons.filter_list),
                SizedBox(
                  width: 2,
                ),
                Text('فیلتر کردن'),
              ],
            ),
            textColor: colorTextWhite,
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [_getScalarList(), _getLineChart()],
          ),
        ),
      ),
    );
  }

  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }

  Future<List<KPIScalarChart>> _postGetScalarCharts(
      String accessToken, bool isFixed) async {
    // set up POST request arguments
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken',
      "Content-type": "application/json"
    };
    String postInfo;
    if (isFixed) {
      postInfo =
          '{"request":{"unit":"$unit","datetime_fix": "$datetimeFix","app_status": "$appStatus","calculationCriterion": "$calculationCriterion"	}}';
    } else {
      postInfo =
          '{"request":{"unit":"$unit","begin_date": "$beginDate","end_date": "$endDate","app_status": "$appStatus","calculationCriterion": "$calculationCriterion"	}}';
    }

    String url = apiServer + 'mobiledashboard/kpi-scalar-process';
    String json = postInfo;
    // make POST request
    http.Response response = http.Response('{}', 200);

    try {
      response = await http.post(url, headers: requestHeaders, body: json);
      //print('scalar charts reponse : ' + response.body);
      if (response.statusCode == 200) {
        var json = jsonDecode(response.body);
        //print('json["max"]' + json['max'].toString());
        List<KPIScalarChart> listOfKPIScalars = List();
        listOfKPIScalars.add(KPIScalarChart(
          value: (json['max']['value'] ?? 0).toDouble(),
          title: json['max']['title'] ?? ' ',
          secondTitle: json['max']['second_title'] ?? ' ',
          type: json['max']['type'] ?? ' ',
          icon: json['max']['icon'] ?? ' ',
        ));
        listOfKPIScalars.add(KPIScalarChart(
          value: (json['min']['value'] ?? 0).toDouble(),
          title: json['min']['title'] ?? ' ',
          secondTitle: json['max']['second_title'] ?? ' ',
          type: json['min']['type'] ?? ' ',
          icon: json['min']['icon'] ?? ' ',
        ));
        listOfKPIScalars.add(KPIScalarChart(
          value: (json['avg']['value'] ?? 0).toDouble(),
          title: json['avg']['title'] ?? ' ',
          secondTitle: json['avg']['second_title'] ?? ' ',
          type: json['avg']['type'] ?? ' ',
          icon: json['avg']['icon'] ?? ' ',
        ));
        listOfKPIScalars.add(KPIScalarChart(
          value: (json['count']['value'] ?? 0).toDouble(),
          title: json['count']['title'] ?? ' ',
          secondTitle: json['count']['second_title'] ?? ' ',
          type: json['count']['type'] ?? ' ',
          icon: json['count']['icon'] ?? ' ',
        ));

        //print('listOfDashboardItems[listOfDashboardItems.length-1].id = ' + listOfDashboardItems[listOfDashboardItems.length-1].id);
        return listOfKPIScalars;
      } else {
        print('failed' + response.statusCode.toString());
        throw Exception('failed error' + response.statusCode.toString());
      }
    } catch (e) {
      print('failed');
      throw Exception(e);
    }
  }

  Widget _getScalarList() {
    var widget = Container(
      alignment: Alignment.topCenter,
      padding: const EdgeInsets.all(8.0),
      child: Wrap(
        spacing: 16,
        children: _kpiScalarCharts,
      ),
    );
    return _isScalarChartsLoaded
        ? widget
        : Center(child: CircularProgressIndicator());
  }

  Future<bool> _getBarLineCharts(accessToken, bool isFixed) async {
    List<ChartInfo> charts = List();
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken',
      "Content-type": "application/json"
    };
    String postInfo;
    if (isFixed) {
      postInfo =
          '{"request":{"unit":"$unit","datetime_fix": "$datetimeFix","app_status": "$appStatus","calculationCriterion": "$calculationCriterion"	}}';
    } else {
      postInfo =
          '{"request":{"unit":"$unit","begin_date": "$beginDate","end_date": "$endDate","app_status": "$appStatus","calculationCriterion": "$calculationCriterion"	}}';
    }
    String url = apiServer + 'mobiledashboard/kpi-all-process';
    print('url = ' + url);

    http.Response response = http.Response('{}', 200);

    //print('respons body = ' + response.body);
    try {
      response = await http.post(url, headers: requestHeaders, body: postInfo);
    } catch (e) {
      print('failed' + e.toString());
      return false;
    }
    //print('response.body bar line = ' + response.body);
    var json = jsonDecode(response.body);

    ChartInfo chart;

    chart = ChartInfo(
      title: json['line']['title'],
      type: json['line']['type'],
      domainTitle: json['line']['domain-title'],
      measureTitle: json['line']['measure-title'],
      measures: json['line']['measure'],
    );
    charts.add(chart);
    _lineChart = KPILineChart(
      chartsInfo: chart,
    );

    print('title' + chart.title);
    chart = ChartInfo(
      title: json['column']['title'],
      type: json['column']['type'],
      domainTitle: json['column']['domain-title'],
      measureTitle: json['column']['measure-title'],
      measures: json['column']['measure'],
    );
    charts.add(chart);
    _barChart = KPIBarChart(
      chartsInfo: chart,
    );

    print('title' + chart.title);
    //print('chart measure = ' + chart.measures.values.toList()[0].toString());
    //print('measures ' + chart.measures[0][1].toString());

    return true;
  }

  void _showFilterDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) => AnimationHandler()
            .popUp(FilterDialog(callback: callback), Curves.easeOutBack),
        barrierDismissible: true);
  }

  //this methodwas rushed, I am no proud of this method, I hate myself for writing it
  void callback(
    bool isFixed,
    String u,
    String a,
    String c,
    String dateTimeFixed,
    String fDate,
    String tDate,
  ) {
    if (isFixed) {
      setState(() {
        _lineChart = KPILineChart();
        _barChart = KPIBarChart();
        _kpiScalarCharts = List();
        _isScalarChartsLoaded = false;
        _isBarLineChartsLoaded = false;
        unit = u;
        datetimeFix = dateTimeFixed;
        appStatus = a;
        calculationCriterion = c;
        endDate = tDate;
        beginDate = fDate;

        getAccessToken().then((accessToken) {
          _postGetScalarCharts(accessToken, true).then((list) {
            setState(() {
              _kpiScalarCharts = list;
              _isScalarChartsLoaded = true;
            });
          });
          _getBarLineCharts(accessToken, true).then((isLoaded) {
            setState(() {
              _isBarLineChartsLoaded = isLoaded;
            });
          });
        });
      });
    } else {
      setState(() {
        _lineChart = KPILineChart();
        _barChart = KPIBarChart();
        _kpiScalarCharts = List();
        _isScalarChartsLoaded = false;
        _isBarLineChartsLoaded = false;
        unit = u;
        datetimeFix = dateTimeFixed;
        appStatus = a;
        calculationCriterion = c;
        endDate = tDate;
        beginDate = fDate;

        getAccessToken().then((accessToken) {
          _postGetScalarCharts(accessToken, false).then((list) {
            setState(() {
              _kpiScalarCharts = list;
              _isScalarChartsLoaded = true;
            });
          });
          _getBarLineCharts(accessToken, false).then((isLoaded) {
            setState(() {
              _isBarLineChartsLoaded = isLoaded;
            });
          });
        });
      });
    }
  }

  Widget _getLineChart() {
    Widget widget = Center(
      child: Padding(
        padding: const EdgeInsets.all(32.0),
        child: CircularProgressIndicator(),
      ),
    );
    if (_isBarLineChartsLoaded) {
      widget = Directionality(
        textDirection: TextDirection.ltr,
        child: new Builder(
          builder: (BuildContext context) {
            return new MediaQuery(
                data: MediaQuery.of(context).copyWith(
                  textScaleFactor: 1.0,
                ),
                child: Column(
                  children: <Widget>[_lineChart, _barChart],
                ));
          },
        ),
      );
    }
    return widget;
  }
}
