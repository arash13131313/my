import 'package:flutter/material.dart';
import 'package:my/GlobalData.dart';
import 'package:persian_datepicker/persian_datepicker.dart';
import 'package:persian_datepicker/persian_datetime.dart';

class FilterDialog extends StatefulWidget {
  final Function callback;

  FilterDialog({Key key, this.callback}) : super(key: key);
  @override
  _FilterDialogState createState() => _FilterDialogState();
}

class _FilterDialogState extends State<FilterDialog> {
  @override
  void initState() {
    super.initState();
    _groupValue = 0;
    persianDatePickerFrom = PersianDatePicker(
      controller: _controllerFromDate,
      datetime: DateTime.now().toIso8601String(),
      fontFamily: 'Sans',
      outputFormat: 'YYYY/MM/DD',
      weekCaptionsBackgroundColor: colorPrimary,
      dayBlockRadius: 5,
    ).init();
    persianDatePickerTo = PersianDatePicker(
      controller: _controllerToDate,
      datetime: DateTime.now().toString(),
      outputFormat: 'YYYY/MM/DD',
      fontFamily: 'Sans',
      weekCaptionsBackgroundColor: colorPrimary,
      dayBlockRadius: 5,
    ).init();
  }

  TextEditingController _controllerFromDate = TextEditingController();
  TextEditingController _controllerToDate = TextEditingController();
  PersianDatePickerWidget persianDatePickerFrom;
  PersianDatePickerWidget persianDatePickerTo;
  List<String> _units = ['ثانیه', 'دقیقه', 'ساعت', 'روز', 'هفته', 'ماه', 'سال'];
  List<String> _status = ['تمام شده', 'در حال انجام'];
  String _selectedUnit;
  String _selectedCriterion;
  String _selectedStatus;

  int _groupValue;
  double _fixDateValue = 0;
  String _fixDateString;
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(12.0)), //this right here
      child: Container(
        padding: EdgeInsets.all(textFontSizeTitle  - 1),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.filter_list),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'مرتب سازی',
                    style: TextStyle(fontSize: textFontSizeTitle  + 1, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Radio(
                  value: 0,
                  groupValue: _groupValue,
                  activeColor: colorPrimary,
                  onChanged: _handleRadioValueChange1,
                ),
                Text(
                  'بازه زمانی',
                  style: _groupValue == 0
                      ? TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle  - 1)
                      : TextStyle(
                          color: colorTextSub2,
                          fontWeight: FontWeight.normal,
                          fontSize: textFontSizeSub  + 2),
                ),
                Expanded(child: Container()),
                Text(
                  _getFixDateString(_fixDateValue.toInt()),
                  style: _groupValue == 0
                      ? TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.normal,
                          fontSize: textFontSizeSub  + 2)
                      : TextStyle(
                          color: colorTextSub2,
                          fontWeight: FontWeight.normal,
                          fontSize: textFontSizeSub + 1),
                ),
                Expanded(child: Container()),
              ],
            ),
            AbsorbPointer(
              absorbing: false,
              child: Container(
                child: Slider(
                  max: 8,
                  min: 0,
                  activeColor:
                      _groupValue == 0 ? colorPrimary : colorDeactivated,
                  value: _fixDateValue,
                  onChanged: _groupValue == 0
                      ? (newValue) {
                          setState(() {
                            _fixDateValue = newValue;
                            _fixDateString =
                                _getFixDateString(_fixDateValue.toInt());
                          });
                        }
                      : null,
                  divisions: 9,
                  label: _getFixDateString(_fixDateValue.toInt()),
                ),
              ),
            ),
            Row(
              children: <Widget>[
                Radio(
                  value: 1,
                  groupValue: _groupValue,
                  activeColor: colorPrimary,
                  onChanged: _handleRadioValueChange1,
                ),
                Text(
                  'تاریخ',
                  style: _groupValue == 1
                      ? TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle  - 1)
                      : TextStyle(
                          color: colorTextSub2,
                          fontWeight: FontWeight.normal,
                          fontSize: textFontSizeSub  + 2),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  child: TextFormField(
                    enabled: _groupValue == 1,
                    onTap: () {
                      FocusScope.of(context).requestFocus(
                          new FocusNode()); // to prevent opening default keyboard
                      showModalBottomSheet(
                          context: context,
                          builder: (BuildContext context) {
                            return persianDatePickerFrom;
                          });
                    },
                    enableInteractiveSelection: false,
                    //keyboardType: TextInputType.number,

                    controller: _controllerFromDate,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        isDense: true,
                        labelText: 'از تاریخ'),
                    style: _groupValue == 1
                        ? TextStyle(
                            fontSize: textFontSizeTitle  - 1.0,
                            color: colorTextPrimary,
                            height: 1,
                          )
                        : TextStyle(
                            fontSize: textFontSizeSub  + 2.0,
                            color: colorTextSub2,
                            height: 1,
                          ),
                  ),
                ),
                SizedBox(
                  width: 4,
                ),
                Expanded(
                  child: TextFormField(
                    enabled: _groupValue == 1,
                    onTap: () {
                      FocusScope.of(context).requestFocus(
                          new FocusNode()); // to prevent opening default keyboard
                      showModalBottomSheet(
                          context: context,
                          builder: (BuildContext context) {
                            return persianDatePickerTo;
                          });
                    },
                    enableInteractiveSelection: false,
                    //keyboardType: TextInputType.number,

                    controller: _controllerToDate,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        isDense: true,
                        labelText: 'تا تاریخ'),
                    style: _groupValue == 1
                        ? TextStyle(
                            fontSize: textFontSizeTitle  - 1.0,
                            color: colorTextPrimary,
                            height: 1,
                          )
                        : TextStyle(
                            fontSize: textFontSizeSub  + 2.0,
                            color: colorTextSub2,
                            height: 1,
                          ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8, right: 16, left:16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'واحد:',
                    style: TextStyle(fontSize: textFontSizeTitle  - 1, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(width: 4),
                  DropdownButton(
                    hint: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Text('یک واحد زمانی را انتخاب کنید'),
                    ), // Not necessary for Option 1
                    value: _selectedUnit,

                    onChanged: (newValue) {
                      setState(() {
                        _selectedUnit = newValue;
                      });
                    },
                    items: _units.map((location) {
                      return DropdownMenuItem(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.access_alarm,
                                size: textFontSizeTitle  - 1,
                              ),
                              SizedBox(width: 4),
                              Text(location, style: TextStyle(fontSize: textFontSizeTitle  - 1)),
                            ],
                          ),
                        ),
                        value: location,
                      );
                    }).toList(),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(bottom: 16, right: 16, left: 16),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'وضعیت:',
                    style: TextStyle(fontSize: textFontSizeTitle  - 1, fontWeight: FontWeight.bold),
                  ),
                  SizedBox(width: 8),
                  DropdownButton(
                    hint: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Text('وضعیت را انتخاب کنید'),
                    ), // Not necessary for Option 1
                    value: _selectedStatus,

                    onChanged: (newValue) {
                      setState(() {
                        _selectedStatus = newValue;
                      });
                    },
                    items: _status.map((location) {
                      return DropdownMenuItem(
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.list,
                                size: textFontSizeTitle  - 1,
                              ),
                              SizedBox(width: 4),
                              Text(location, style: TextStyle(fontSize: textFontSizeTitle  - 1)),
                            ],
                          ),
                        ),
                        value: location,
                      );
                    }).toList(),
                  )
                ],
              ),
            ),
            
            FlatButton(
              onPressed: () {
                _applyFilters();
                Navigator.of(context).pop();
              },
              child: Text(
                'اعمال',
                style: TextStyle(color: colorPrimary, fontSize: textFontSizeTitle  + 1),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _applyFilters() {
    //print('fixDate = $_fixDateString');
    PersianDateTime dateFrom = PersianDateTime(
        jalaaliDateTime: _controllerFromDate.text
            .replaceAll('۱', '1')
            .replaceAll('۲', '2')
            .replaceAll('۳', '3')
            .replaceAll('۴', '4')
            .replaceAll('۵', '5')
            .replaceAll('۶', '6')
            .replaceAll('۷', '7')
            .replaceAll('۸', '8')
            .replaceAll('۹', '9')
            .replaceAll('۰', '0'));
    PersianDateTime dateTo = PersianDateTime(
        jalaaliDateTime: _controllerToDate.text
            .replaceAll('۱', '1')
            .replaceAll('۲', '2')
            .replaceAll('۳', '3')
            .replaceAll('۴', '4')
            .replaceAll('۵', '5')
            .replaceAll('۶', '6')
            .replaceAll('۷', '7')
            .replaceAll('۸', '8')
            .replaceAll('۹', '9')
            .replaceAll('۰', '0'));
    String dateFromGregorian = dateFrom.toGregorian(format: 'YYYY/MM/DD');
    String dateToGregorian = dateTo.toGregorian(format: 'YYYY/MM/DD');
    // print('date from ' + dateFromGregorian + ' to ' + dateToGregorian);

    // print('unit = $_selectedUnit = ' + _getSelectedUniValue(_selectedUnit));
    // print('status = $_selectedStatus');
    if (_groupValue == 0) {
      widget.callback(
        true,
        _getSelectedUniValue(_selectedUnit),
        _getAppStatues(_selectedStatus),
        _getCalculationCriterion(_selectedCriterion),
        _getFixDate(_fixDateValue.toInt()),
        dateFromGregorian,
        dateToGregorian
      );
    } else {
      widget.callback(
        false,
        _getSelectedUniValue(_selectedUnit),
        _getAppStatues(_selectedStatus),
        _getCalculationCriterion(_selectedCriterion),
        _getFixDate(_fixDateValue.toInt()),
        dateFromGregorian,
        dateToGregorian
      );
    }
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _groupValue = value;
    });
  }

  String _getFixDateString(int value) {
    switch (value) {
      case 0:
        _fixDateString = '۱ ساعت اخیر';
        break;
      case 1:
        _fixDateString = '۶ ساعت اخیر';
        break;
      case 2:
        _fixDateString = '۱۲ ساعت اخیر';
        break;
      case 3:
        _fixDateString = '۱ روز گذشته';
        break;
      case 4:
        _fixDateString = '۱ هفته گذشته';
        break;
      case 5:
        _fixDateString = '۱ ماه گذشته';
        break;
      case 6:
        _fixDateString = '۳ ماه گذشته';
        break;
      case 7:
        _fixDateString = '۶ ماه گذشته';
        break;
      case 8:
        _fixDateString = '۱ سال گذشته';
        break;
    }
    return _fixDateString;
  }

  String _getSelectedUniValue(String selectedUnit) {
    switch (selectedUnit) {
      case 'ثانیه':
        return 'second';
      case 'دقیقه':
        return 'minute';
      case 'ساعت':
        return 'hour';
      case 'روز':
        return 'day';
      case 'هفته':
        return 'week';
      case 'ماه':
        return 'month';
      case 'سال':
        return 'year';
      default:
        return 'day';
    }
  }

  String _getAppStatues(String selectedStatus) {
    switch (selectedStatus) {
      case 'تمام شده':
        return 'COMPLETED';
      case 'در حال انجام':
        return 'TO_DO';
      default:
        return 'COMPLETED';
    }
  }

  String _getCalculationCriterion(String selectedCriterion) {
    switch (selectedCriterion) {
      case 'حداقل':
        return 'min';
      case 'حداکثر':
        return 'max';
      case 'میانگین':
        return 'avg';
      case 'تعداد':
        return 'count';
      default:
        return 'min';
    }
  }

  String _getFixDate(int fixDateValue) {
    switch (fixDateValue) {
      case 0:
        return '1 hour';
        break;
      case 1:
        return '6 hour';
        break;
      case 2:
        return '12 hour';
        break;
      case 3:
        return '1 day';
        break;
      case 4:
        return '7 day';
        break;
      case 5:
        return '1 month';
        break;
      case 6:
        return '3 month';
        break;
      case 7:
        return '6 month';
        break;
      case 8:
        return '12 month';
        break;
      default:
        return '1 month';
    }
  }
}
