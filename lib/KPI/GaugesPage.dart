import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:my/KPI/FilterDialog.dart';
import 'package:my/KPI/KPIGaugeChart.dart';
import 'package:my/utils/AnimationHandler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import '../GlobalData.dart';

class GaugesPage extends StatefulWidget {
  final int id;
  final String title;

  const GaugesPage({Key key, this.id, this.title}) : super(key: key);
  @override
  _GaugesPageState createState() => _GaugesPageState();
}

class _GaugesPageState extends State<GaugesPage> {
  List<KPIGaugeChart> _kpiGaugeList = List();
  bool _isLoaded = false;
  String unit, datetimeFix, appStatus, calculationCriterion, beginDate, endDate;
  int kpiTypeId;
  @override
  void initState() {
    super.initState();
    _isLoaded = false;
    _kpiGaugeList = List();
    unit = "day";
    datetimeFix = "1 month";
    appStatus = "COMPLETED";
    calculationCriterion = "Min";
    beginDate = " ";
    endDate = " ";
    kpiTypeId = widget.id;
    getAccessToken().then((accessToken) {
      _fetchKPIItems(accessToken).then((list) {
        setState(() {
          _kpiGaugeList = list;
          _isLoaded = true;
        });
      });
    });
  }

  Future<List<KPIGaugeChart>> _fetchKPIItems(accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };

    String url =
        apiServer + 'mobiledashboard/kpi-dashlet-list/' + widget.id.toString();
    //print('requestHeadersNewTAsks = ' + requestHeaders.toString());
    var response = await http.get(url, headers: requestHeaders);
    print('responsebody kpi-list = ' + response.body);
    if (response.statusCode == 200) {
      //print('response body dashboard-list = ' + response.body);
      final items = jsonDecode(response.body).cast<Map<String, dynamic>>();
      List<KPIGaugeChart> listOfKPIGauges = items.map<KPIGaugeChart>((json) {
        return KPIGaugeChart.fromJson(json);
      }).toList();
      //print('listOfDashboardItems[listOfDashboardItems.length-1].id = ' + listOfDashboardItems[listOfDashboardItems.length-1].id);
      return listOfKPIGauges;
    } else if (response.statusCode == 401) {
      print('Unauthorized KPIGauge LIst');
    } else {
      print('Exception');
      throw Exception('Exception');
    }
    return List();
  }

  Future<List<KPIGaugeChart>> _postKPIItemsFixed(accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken',
      "Content-type": "application/json"
    };
    String postInfo =
        '{"unit":"$unit","datetime_fix": "$datetimeFix","app_status": "$appStatus","calculationCriterion": "$calculationCriterion","kpi_type_id": $kpiTypeId	}';

    String url = apiServer + 'mobiledashboard/kpi-dashlet-list/';
    //print('requestHeadersNewTAsks = ' + requestHeaders.toString());
    var response =
        await http.post(url, headers: requestHeaders, body: postInfo);
    print('responsebody kpi-list = ' + response.body);
    if (response.statusCode == 200) {
      //print('response body dashboard-list = ' + response.body);
      final items = jsonDecode(response.body).cast<Map<String, dynamic>>();
      List<KPIGaugeChart> listOfKPIGauges = items.map<KPIGaugeChart>((json) {
        return KPIGaugeChart.fromJson(json);
      }).toList();
      //print('listOfDashboardItems[listOfDashboardItems.length-1].id = ' + listOfDashboardItems[listOfDashboardItems.length-1].id);
      return listOfKPIGauges;
    } else if (response.statusCode == 401) {
      print('Unauthorized KPIGauge LIst');
    } else {
      print('Exception');
      throw Exception('Exception');
    }
    return List();
  }

  Future<List<KPIGaugeChart>> _postKPIItemsNotFixed(accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken',
      "Content-type": "application/json"
    };
    String postInfo =
        '{"unit":"$unit","begin_date": "$beginDate","end_date": "$endDate","app_status": "$appStatus","calculationCriterion": "$calculationCriterion","kpi_type_id": $kpiTypeId	}';

    String url = apiServer + 'mobiledashboard/kpi-dashlet-list/';
    //print('requestHeadersNewTAsks = ' + requestHeaders.toString());
    var response =
        await http.post(url, headers: requestHeaders, body: postInfo);
    print('responsebody kpi-list = ' + response.body);
    if (response.statusCode == 200) {
      //print('response body dashboard-list = ' + response.body);
      final items = jsonDecode(response.body).cast<Map<String, dynamic>>();
      List<KPIGaugeChart> listOfKPIGauges = items.map<KPIGaugeChart>((json) {
        return KPIGaugeChart.fromJson(json);
      }).toList();
      //print('listOfDashboardItems[listOfDashboardItems.length-1].id = ' + listOfDashboardItems[listOfDashboardItems.length-1].id);
      return listOfKPIGauges;
    } else if (response.statusCode == 401) {
      print('Unauthorized KPIGauge LIst');
    } else {
      print('Exception');
      throw Exception('Exception');
    }
    return List();
  }

  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorBackground,
      appBar: AppBar(
        title: Text(widget.title),
        backgroundColor: colorAccent,
        actions: <Widget>[
          FlatButton(
            onPressed: () {
              _showFilterDialog();
            },
            child: Row(
              children: <Widget>[
                Icon(Icons.filter_list),
                SizedBox(
                  width: 2,
                ),
                Text('فیلتر کردن'),
              ],
            ),
            textColor: colorTextWhite,
          )
        ],
      ),
      body: _isLoaded
          ? SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.all(8),
                alignment: Alignment.topLeft,
                child: Wrap(
                  children: _kpiGaugeList,
                ),
              ),
            )
          : Center(child: CircularProgressIndicator()),
    );
  }

  void _showFilterDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) => AnimationHandler()
            .popUp(FilterDialog(callback: callback), Curves.easeOutBack),
        barrierDismissible: true);
  }

  void callback(
    bool isFixed,
    String u,
    String a,
    String c,
    String dateTimeFixed,
    String fDate,
    String tDate,
  ) {
    if (isFixed) {
      setState(() {
        _isLoaded = false;
        _kpiGaugeList = List();
        unit = u;
        datetimeFix = dateTimeFixed;
        appStatus = a;
        calculationCriterion = c;
        kpiTypeId = widget.id;
        endDate = tDate;
        beginDate = fDate;
        print(
            'isFixed = $isFixed | unit = $u | datetimeFix = $dateTimeFixed | ' +
                'appStatus = $a | calculationCriterion = $c | widgetid = ' +
                widget.id.toString());
        getAccessToken().then((accessToken) {
          _postKPIItemsFixed(accessToken).then((list) {
            setState(() {
              print('got em');
              _kpiGaugeList = list;
              _isLoaded = true;
            });
          });
        });
      });
    } else {
      setState(() {
        _isLoaded = false;
        _kpiGaugeList = List();
        unit = u;
        datetimeFix = dateTimeFixed;
        appStatus = a;
        calculationCriterion = c;
        kpiTypeId = widget.id;
        endDate = tDate;
        beginDate = fDate;
        getAccessToken().then((accessToken) {
          _postKPIItemsNotFixed(accessToken).then((list) {
            setState(() {
              print('got em again');
              _kpiGaugeList = list;
              _isLoaded = true;
            });
          });
        });
      });
    }
  }
}
