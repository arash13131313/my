import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class KPIGaugeChart extends StatefulWidget {
  final double min;
  final double max;
  final double red;
  final double yellow;
  final double orange;
  final double value;
  final String title;
  final String description;
  final String unit;
  final String dateTimeFix;
  final String goal;

  const KPIGaugeChart(
      {Key key,
      this.min,
      this.max,
      this.red,
      this.yellow,
      this.orange,
      this.value,
      this.title,
      this.description,
      this.unit,
      this.dateTimeFix,
      this.goal})
      : super(key: key);

  factory KPIGaugeChart.fromJson(Map<String, dynamic> parsedJson) {
    return KPIGaugeChart(
      min: double.parse(parsedJson['min'] ?? '0'),
      max: double.parse(parsedJson['max'] ?? '0'),
      red: double.parse(parsedJson['critical'] ?? '0'),
      yellow: double.parse(parsedJson['warning'] ?? '0'),
      orange: double.parse(parsedJson['critical'] ?? '0'),
      value: (parsedJson['CurrentValue'] ?? 0).toDouble(),
      title: parsedJson['title'] ?? ' ',
      description: parsedJson['calculationCriterion_label'] ?? ' ',
      unit: parsedJson['unit'] ?? ' ',
      dateTimeFix: parsedJson['datetime_fix'] ?? ' ',
      goal: parsedJson['goal'] ?? ' ',
    );
  }
  @override
  _KPIGaugeChartState createState() => _KPIGaugeChartState();
}

class _KPIGaugeChartState extends State<KPIGaugeChart> {
  double yellow;
  double orange;
  double red;
  double min;
  double max;
  double value;
  String description;
  String unit;
  String dateTimeFix;
  String goal;
  String title;
  @override
  void initState() {
    super.initState();
    yellow = widget.yellow ?? 40;
    orange = widget.orange ?? 60;
    red = widget.red ?? 80;
    min = widget.min ?? 0;
    max = widget.max ?? 100;
    value = widget.value ?? 0;
    description = widget.description ?? ' ';
    unit = widget.unit ?? 'M';
    dateTimeFix = widget.dateTimeFix ?? ' ';
    goal = widget.goal ?? ' ';
    title = widget.title ?? 'نمودار عقربه‌ای';
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      padding: EdgeInsets.only(top: 8, bottom: 1),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(32.0),
            bottomLeft: Radius.circular(12.0),
            bottomRight: Radius.circular(32.0),
            topRight: Radius.circular(12.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              offset: Offset(1.1, 1.1),
              blurRadius: 10.0),
        ],
      ),
      child: Container(
        constraints: BoxConstraints(
            maxWidth: (MediaQuery.of(context).size.width / 2) - 24),
        alignment: Alignment.center,
        padding: EdgeInsets.only(top: 16),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 4),
              child: Text(
                title,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
                textAlign: TextAlign.center,
              ),
            ),
            Divider(
              indent: 16,
              endIndent: 16,
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 16),
              constraints: BoxConstraints(
                  maxWidth: (MediaQuery.of(context).size.width / 2) - 24,
                  maxHeight: 175),
              child: SfRadialGauge(
                axes: <RadialAxis>[
                  RadialAxis(
                    minimum: min ?? 0.0,
                    maximum: max ?? 100.0,
                    radiusFactor: 1,
                    ranges: <GaugeRange>[
                      GaugeRange(
                          startValue: 0, endValue: yellow, color: Colors.green),
                      GaugeRange(
                          startValue: yellow,
                          endValue: orange,
                          color: Colors.amber),
                      GaugeRange(
                          startValue: orange,
                          endValue: red,
                          color: Colors.orange),
                      GaugeRange(
                          startValue: red, endValue: max, color: Colors.red),
                    ],
                    majorTickStyle: MajorTickStyle(
                        length: 0.08,
                        lengthUnit: GaugeSizeUnit.factor,
                        thickness: 4),
                    axisLabelStyle: GaugeTextStyle(
                        fontFamily: 'Sans',
                        fontSize: 12,
                        fontWeight: FontWeight.w800,
                        fontStyle: FontStyle.italic),
                    minorTickStyle: MinorTickStyle(
                      length: 0.04,
                      thickness: 2,
                      lengthUnit: GaugeSizeUnit.factor,
                      color: const Color(0xFFC4C4C4),
                    ),
                    axisLineStyle: AxisLineStyle(
                        color: const Color(0xFFDADADA),
                        thicknessUnit: GaugeSizeUnit.factor,
                        thickness: 0.1),
                    pointers: <GaugePointer>[
                      NeedlePointer(
                        value: value,
                        needleStartWidth: 2,
                        needleEndWidth: 5,
                        needleColor: const Color(0xFFF67280),
                        needleLength: 0.7,
                        lengthUnit: GaugeSizeUnit.factor,
                        enableAnimation: true,
                        animationType: AnimationType.bounceOut,
                        animationDuration: 1500,
                        knobStyle: KnobStyle(
                          knobRadius: 8,
                          sizeUnit: GaugeSizeUnit.logicalPixel,
                          color: const Color(0xFFF67280),
                        ),
                      ),
                    ],
                    annotations: <GaugeAnnotation>[
                      GaugeAnnotation(
                          widget: Container(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  value.toStringAsFixed(2),
                                  style: TextStyle(
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold),
                                ),
                                Text(
                                  unit,
                                  style: TextStyle(
                                      color: Colors.grey[500],
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ),
                          angle: 90,
                          positionFactor: 0.5),
                      GaugeAnnotation(
                          angle: 270,
                          positionFactor: 0.4,
                          widget: Container(
                            child: Column(
                              children: <Widget>[
                                Text(
                                  description,
                                  style: TextStyle(
                                      color: Colors.grey[500],
                                      fontSize: 13,
                                      fontWeight: FontWeight.normal),
                                ),
                                Text(
                                  dateTimeFix,
                                  style: TextStyle(
                                      color: Colors.grey[500],
                                      fontSize: 11,
                                      fontWeight: FontWeight.normal),
                                ),
                              ],
                            ),
                          ))
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class SalesData {
  SalesData(this.year, this.sales);

  final String year;
  final double sales;
}
