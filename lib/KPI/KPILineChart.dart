import 'package:my/Dashboard/ChartInfo.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class KPILineChart extends StatefulWidget {
  final ChartInfo chartsInfo;

  const KPILineChart({this.chartsInfo});
  @override
  _KPILineChartState createState() => _KPILineChartState();
}

class _KPILineChartState extends State<KPILineChart> {
  List<charts.Series<LineChartInfo, String>> _createSampleData() {
    var measureTitles = widget.chartsInfo.measureTitle;
    var domainTitles = widget.chartsInfo.domainTitle;
    var measures = widget.chartsInfo.measures;
    List<LineChartInfo> barChartInfo = List();
    List<List<LineChartInfo>> barChartInfoList = List();
    for (var j = 0; j < domainTitles.length; j++) {
      for (var i = 0; i < measureTitles.length; i++) {
        //print('i = ' + i.toString() + '| j = ' + j.toString());
        barChartInfo.add(
          LineChartInfo(domainTitles[j],
              (measures.values.toList()[i][j] ?? 0.0).toDouble(),
              color: getChartColor((i + 1) * i)),
        );
        //print('barChartInfo[$i] Measure = ' + barChartInfo[i].measure.toString());
      }
    }
    String cur = ' ';
    print('lenf]gh = ' + barChartInfo.length.toString());
    for (var i = 0; i < measureTitles.length; i++) {
      List<LineChartInfo> b = List();
      for (var j = 0; j < barChartInfo.length; j++) {
        if (barChartInfo[j].domain != cur) {
          b.add(barChartInfo[j]);
          cur = barChartInfo[j].domain;
          barChartInfo.removeAt(j);
          j--;
        }
      }
      barChartInfoList.add(b);
    }

    List<charts.Series<LineChartInfo, String>> series = List();

    for (var i = 0; i < barChartInfoList.length; i++) {
      series.add(
        charts.Series<LineChartInfo, String>(
          id: widget.chartsInfo.measures.keys.toList()[i],
          domainFn: (LineChartInfo gross, _) => gross.domain,
          measureFn: (LineChartInfo gross, _) => gross.measure,
          colorFn: (LineChartInfo gross, _) => gross.color,
          data: barChartInfoList[i],
        )..setAttribute(charts.rendererIdKey, 'customLine'),
      );
    }
    // for (var i = 0; i < barChartInfoList.length; i++) {
    //   series.add(
    //     charts.Series<LineChartInfo, String>(
    //       id: ' ',
    //       domainFn: (LineChartInfo gross, _) => gross.domain,
    //       measureFn: (LineChartInfo gross, _) => gross.measure,
    //       colorFn: (LineChartInfo gross, _) => gross.color,
    //       data: barChartInfoList[i],
    //     )..setAttribute(charts.rendererIdKey, 'customPoint'),
    //   );
    // }

    return series;
  }

  String _getStartDomain() {
    String start;
    start = widget.chartsInfo.domainTitle.length > 30
        ? widget
            .chartsInfo.domainTitle[widget.chartsInfo.domainTitle.length - 30]
        : '0';
    print('_getStartDomain = ' + start);
    return start;
  }

  int _getDataLength() {
    int length;
    length = widget.chartsInfo.domainTitle.length > 30
        ? 30
        : widget.chartsInfo.domainTitle.length - 1;
    if (length < 1) {
      length = 1;
    }
    print('_getDataLength = ' + length.toString());
    return length;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 8),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
        padding: EdgeInsets.only(top: 8, bottom: 1),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(12.0),
              bottomLeft: Radius.circular(12.0),
              bottomRight: Radius.circular(12.0),
              topRight: Radius.circular(12.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.withOpacity(0.2),
                offset: Offset(1.1, 1.1),
                blurRadius: 10.0),
          ],
        ),
        child: Container(
          padding: EdgeInsets.all(8),
          width: MediaQuery.of(context).size.width - 12,
          height: 430,
          child: charts.OrdinalComboChart(
            _createSampleData(),
            animate: true,
            animationDuration: Duration(milliseconds: 0),
            domainAxis: charts.OrdinalAxisSpec(
              viewport: new charts.OrdinalViewport(
                  _getStartDomain(), _getDataLength()),
              renderSpec: charts.SmallTickRendererSpec(
                // Tick and Label styling here.
                labelRotation: 45,
                labelStyle: charts.TextStyleSpec(
                    color: charts.MaterialPalette.black, fontFamily: 'Sans'),
                // Change the line colors to match text color.
                lineStyle:
                    charts.LineStyleSpec(color: charts.MaterialPalette.black),
              ),
            ),
            primaryMeasureAxis: charts.NumericAxisSpec(
              tickProviderSpec: charts.BasicNumericTickProviderSpec(
                //desiredMinTickCount: 150000,
                zeroBound: false,
              ),
            ),
            behaviors: [
              charts.ChartTitle(widget.chartsInfo.title),
              new charts.InitialHintBehavior(maxHintTranslate: 4.0),
              new charts.SeriesLegend(
                showMeasures: true,
                horizontalFirst: false,
                position: charts.BehaviorPosition.bottom,
                desiredMaxRows:
                    generrateDesiredRows(widget.chartsInfo.measureTitle.length),
                outsideJustification:
                    charts.OutsideJustification.middleDrawArea,
                entryTextStyle: charts.TextStyleSpec(
                    color: charts.Color(r: 0, g: 0, b: 0),
                    fontFamily: 'Sans',
                    fontSize: 11),
              ),
              new charts.PanAndZoomBehavior(),
              new charts.LinePointHighlighter(
                  showHorizontalFollowLine:
                      charts.LinePointHighlighterFollowLineType.none,
                  showVerticalFollowLine:
                      charts.LinePointHighlighterFollowLineType.nearest),
            ],
            // Configure the default renderer as a bar renderer.
            defaultRenderer: charts.PointRendererConfig<String>(
                symbolRenderer: charts.CircleSymbolRenderer()),
            customSeriesRenderers: [
              charts.LineRendererConfig(customRendererId: 'customLine'),
              charts.PointRendererConfig(customRendererId: 'customPoint')
            ],
          ),
        ),
      ),
    );
  }
}
