import 'package:flutter/material.dart';
import 'package:icons_helper/icons_helper.dart';

import '../GlobalData.dart';

class KPIScalarChart extends StatelessWidget {
  final String title;
  final String secondTitle;
  final String type;
  final double value;
  final String icon;
  factory KPIScalarChart.fromJson(Map<String, dynamic> parsedJson) {
    return KPIScalarChart(
      value: (parsedJson['value'] ?? 0).toDouble(),
      title: parsedJson['title'] ?? ' ',
      secondTitle: parsedJson['second_title'] ?? ' ',
      type: parsedJson['type'] ?? ' ',
      icon: parsedJson['icon'] ?? ' ',
    );
  }

  const KPIScalarChart(
      {Key key, this.title, this.secondTitle, this.type, this.value, this.icon})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    String iconName;
    double d = 2.5;
    double ar = 3 / 4;
    TextStyle style = TextStyle(color: colorTextWhite, fontSize: 18);
    print('icon = ' + icon.toString());
    if (icon == null || icon == '' || icon == ' ') {
      String str = 'fa-clock';
      iconName = str.substring(3, str.length);
    } else {
      iconName = icon.substring(3, icon.length);
    }

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 0),
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
        padding: EdgeInsets.only(right: 2, bottom: 4),
        decoration: BoxDecoration(
          color: colorPrimary,
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(32.0),
              bottomLeft: Radius.circular(12.0),
              bottomRight: Radius.circular(32.0),
              topRight: Radius.circular(12.0)),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.withOpacity(0.6),
                offset: Offset(1.1, 1.1),
                blurRadius: 5.0),
          ],
        ),
        child: Container(
          width: MediaQuery.of(context).size.width / d,
          height: (ar) * MediaQuery.of(context).size.width / d,
          padding: EdgeInsets.all(0),
          child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(32.0),
                      bottomLeft: Radius.circular(12.0),
                      bottomRight: Radius.circular(32.0),
                      topRight: Radius.circular(12.0)),
                  color: Colors.deepOrangeAccent.shade100,
                ),
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width / d,
                height: (ar) * MediaQuery.of(context).size.width / d,
                child: Icon(
                  getIconGuessFavorFA(
                      name: iconName == 'money' ? 'moneyBill' : iconName),
                  size: MediaQuery.of(context).size.width / d / 2,
                  color: Colors.white,
                ),
              ),
              Container(
                padding: EdgeInsets.all(8),
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width / (d - 0.2),
                height: (ar) * MediaQuery.of(context).size.width / (d - 0.2),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(32.0),
                      bottomLeft: Radius.circular(12.0),
                      bottomRight: Radius.circular(32.0),
                      topRight: Radius.circular(12.0)),
                  color: Colors.deepOrange.withOpacity(0.7),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(title, style: style),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(value.toStringAsFixed(2), style: style),
                    ),
                    Text(secondTitle, style: style),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
