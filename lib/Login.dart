import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my/style/theme.dart' as Theme;
import 'package:my/utils/bubble_indication_painter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

import 'GlobalData.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final FocusNode myFocusNodeUNameLogin = FocusNode();
  final FocusNode myFocusNodePasswordLogin = FocusNode();

  final FocusNode myFocusNodeNCode = FocusNode();
  final FocusNode myFocusNodePhone = FocusNode();
  final FocusNode myFocusNodeFName = FocusNode();
  final FocusNode myFocusNodeLName = FocusNode();
  final FocusNode myFocusNodeEmail = FocusNode();

  TextEditingController loginUNameController = new TextEditingController();
  TextEditingController loginPasswordController = new TextEditingController();

  bool _obscureTextLogin = true;

  TextEditingController signupNCodeController = new TextEditingController();
  TextEditingController signupPhoneController = new TextEditingController();
  TextEditingController signupFNameController = new TextEditingController();
  TextEditingController signupLNameController = new TextEditingController();
  TextEditingController signupEmailController = new TextEditingController();

  PageController _pageController;

  Color left = colorTextPrimary;
  Color right = colorTextWhite;
  int pageNum = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: NotificationListener<OverscrollIndicatorNotification>(
        onNotification: (overscroll) {
          overscroll.disallowGlow();
          return null;
        },
        child: SingleChildScrollView(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: pageNum == 0 ? 720 : 860,
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  colors: [
                    Theme.Colors.loginGradientStart,
                    Theme.Colors.loginGradientEnd
                  ],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(1.0, 1.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top: 40.0),
                  child: Hero(
                    tag: 'arianLogo',
                    child: new Image(
                        //width: 190,
                        color: Colors.white,
                        height: 190.0,
                        fit: BoxFit.fill,
                        image: new AssetImage('images/login_icon.png')),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 20.0),
                  child: _buildMenuBar(context),
                ),
                Expanded(
                  flex: 2,
                  child: PageView(
                    controller: _pageController,
                    onPageChanged: (i) {
                      if (i == 0) {
                        setState(() {
                          right = colorTextWhite;
                          left = colorTextPrimary;
                          pageNum = 0;
                        });
                      } else if (i == 1) {
                        setState(() {
                          right = colorTextPrimary;
                          left = colorTextWhite;
                          pageNum = 1;
                        });
                      }
                    },
                    children: <Widget>[
                      new ConstrainedBox(
                        constraints: const BoxConstraints.expand(),
                        child: _buildSignIn(context),
                      ),
                      new ConstrainedBox(
                        constraints: const BoxConstraints.expand(),
                        child: _buildSignUp(context),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    myFocusNodeNCode.dispose();
    myFocusNodePhone.dispose();
    myFocusNodeEmail.dispose();
    myFocusNodeFName.dispose();
    myFocusNodeLName.dispose();
    _pageController?.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    _pageController = PageController();
  }

  void showInSnackBar(String value) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.center,
        style:
            TextStyle(color: colorTextWhite, fontSize: textFontSizeTitle -1, fontFamily: "Sans"),
      ),
      backgroundColor: colorPrimary,
      duration: Duration(seconds: 3),
    ));
  }

  Widget _buildMenuBar(BuildContext context) {
    return Container(
      width: 300.0,
      height: 50.0,
      decoration: BoxDecoration(
        color: Color(0x552B2B2B),
        borderRadius: BorderRadius.all(Radius.circular(25.0)),
      ),
      child: CustomPaint(
        painter: TabIndicationPainter(pageController: _pageController),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignInButtonPress,
                child: Text(
                  "ورود",
                  style: TextStyle(
                    color: left,
                    fontSize: textFontSizeTitle-1,
                  ),
                ),
              ),
            ),
            //Container(height: 33.0, width: 1.0, color: Colors.white),
            Expanded(
              child: FlatButton(
                splashColor: Colors.transparent,
                highlightColor: Colors.transparent,
                onPressed: _onSignUpButtonPress,
                child: Text(
                  "ثبت‌نام",
                  style: TextStyle(
                    color: right,
                    fontSize: textFontSizeTitle -1,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildSignIn(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 190.0,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: myFocusNodeUNameLogin,
                          controller: loginUNameController,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(fontSize: textFontSizeTitle -1, color: colorTextPrimary),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              Icons.person,
                              color: colorTextPrimary,
                              size: 22.0,
                            ),
                            hintText: "نام کاربری",
                            hintStyle: TextStyle(fontSize: textFontSizeTitle -1),
                          ),
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 1.0,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: myFocusNodePasswordLogin,
                          controller: loginPasswordController,
                          obscureText: _obscureTextLogin,
                          style: TextStyle(fontSize: textFontSizeTitle -1, color: colorTextPrimary),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.lock,
                              size: 22.0,
                              color: colorTextPrimary,
                            ),
                            hintText: "رمز ورود",
                            hintStyle: TextStyle(fontSize: textFontSizeTitle -1),
                            suffixIcon: GestureDetector(
                              onTap: _toggleLogin,
                              child: Icon(
                                FontAwesomeIcons.eye,
                                size: 15.0,
                                color: colorTextPrimary,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 170.0),
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Theme.Colors.loginGradientStart,
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                    BoxShadow(
                      color: Theme.Colors.loginGradientEnd,
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                  ],
                  gradient: new LinearGradient(
                      colors: [
                        Theme.Colors.loginGradientEnd,
                        Theme.Colors.loginGradientStart
                      ],
                      begin: const FractionalOffset(0.2, 0.2),
                      end: const FractionalOffset(1.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: MaterialButton(
                    highlightColor: Colors.transparent,
                    splashColor: Theme.Colors.loginGradientEnd,
                    //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 42.0),
                      child: Text(
                        "ورود",
                        style: TextStyle(
                          color: colorTextWhite,
                          fontSize: 25.0,
                        ),
                      ),
                    ),
                    onPressed: () {
                      login();
                    }),
              ),
            ],
          ),
          // Padding(
          //   padding: EdgeInsets.only(top: 10.0),
          //   child: FlatButton(
          //       onPressed: () {},
          //       child: Text(
          //         "رمزو یادت رفته؟",
          //         style: TextStyle(
          //             decoration: TextDecoration.underline,
          //             color: Colors.white,
          //             fontSize: textFontSizeTitle -1,),
          //       )),
          // ),
          Padding(
            padding: EdgeInsets.only(top: 10.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    gradient: new LinearGradient(
                        colors: [
                          Colors.white10,
                          Colors.white,
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 1.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp),
                  ),
                  width: 100.0,
                  height: 1.0,
                ),
                Container(
                  decoration: BoxDecoration(
                    gradient: new LinearGradient(
                        colors: [
                          Colors.white,
                          Colors.white10,
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 1.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp),
                  ),
                  width: 100.0,
                  height: 1.0,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildSignUp(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 23.0),
      child: Column(
        children: <Widget>[
          Stack(
            alignment: Alignment.topCenter,
            overflow: Overflow.visible,
            children: <Widget>[
              Card(
                elevation: 2.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                child: Container(
                  width: 300.0,
                  height: 480.0,
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: myFocusNodeNCode,
                          controller: signupNCodeController,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(fontSize: textFontSizeTitle -1, color: colorTextPrimary),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              Icons.person,
                              color: colorTextPrimary,
                              size: 22.0,
                            ),
                            hintText: "کد ملی",
                            hintStyle: TextStyle(fontSize: textFontSizeTitle -1),
                          ),
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 1.0,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: myFocusNodePhone,
                          controller: signupPhoneController,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(fontSize: textFontSizeTitle -1, color: colorTextPrimary),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.phone,
                              color: colorTextPrimary,
                              size: 22.0,
                            ),
                            hintText: "شماره همراه",
                            hintStyle: TextStyle(fontSize: textFontSizeTitle -1),
                          ),
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 1.0,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: myFocusNodeFName,
                          controller: signupFNameController,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(fontSize: textFontSizeTitle -1, color: colorTextPrimary),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              Icons.person,
                              color: colorTextPrimary,
                              size: 22.0,
                            ),
                            hintText: "نام",
                            hintStyle: TextStyle(fontSize: textFontSizeTitle -1),
                          ),
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 1.0,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: myFocusNodeLName,
                          controller: signupLNameController,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(fontSize: textFontSizeTitle -1, color: colorTextPrimary),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              Icons.person,
                              color: colorTextPrimary,
                              size: 22.0,
                            ),
                            hintText: "نام خانوادگی",
                            hintStyle: TextStyle(fontSize: textFontSizeTitle -1),
                          ),
                        ),
                      ),
                      Container(
                        width: 250.0,
                        height: 1.0,
                        color: Colors.grey[400],
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 20.0, bottom: 20.0, left: 25.0, right: 25.0),
                        child: TextField(
                          focusNode: myFocusNodeEmail,
                          controller: signupEmailController,
                          keyboardType: TextInputType.emailAddress,
                          style: TextStyle(fontSize: textFontSizeTitle -1, color: colorTextPrimary),
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            icon: Icon(
                              FontAwesomeIcons.envelope,
                              color: colorTextPrimary,
                              size: 22.0,
                            ),
                            hintText: "ایمیل",
                            hintStyle: TextStyle(fontSize: textFontSizeTitle -1),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 460.0),
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Theme.Colors.loginGradientStart,
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                    BoxShadow(
                      color: Theme.Colors.loginGradientEnd,
                      offset: Offset(1.0, 6.0),
                      blurRadius: 20.0,
                    ),
                  ],
                  gradient: new LinearGradient(
                      colors: [
                        Theme.Colors.loginGradientEnd,
                        Theme.Colors.loginGradientStart
                      ],
                      begin: const FractionalOffset(0.2, 0.2),
                      end: const FractionalOffset(1.0, 1.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: MaterialButton(
                    highlightColor: Colors.transparent,
                    splashColor: Theme.Colors.loginGradientEnd,
                    //shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(5.0))),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 42.0),
                      child: Text(
                        "ثبت نام",
                        style: TextStyle(
                          color: colorTextWhite,
                          fontSize: 25.0,
                        ),
                      ),
                    ),
                    onPressed: () {
                      signUp();
                    }),
              ),
            ],
          ),
        ],
      ),
    );
  }

  void _onSignInButtonPress() {
    _pageController.animateToPage(0,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void _onSignUpButtonPress() {
    _pageController?.animateToPage(1,
        duration: Duration(milliseconds: 500), curve: Curves.decelerate);
  }

  void _toggleLogin() {
    setState(() {
      _obscureTextLogin = !_obscureTextLogin;
    });
  }

  void login() {
    String name = loginUNameController.text;
    String pass = loginPasswordController.text;
    if (name == "" || pass == "") {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
        duration: Duration(seconds: 3),
        content: Text(
          "  لطفا اطلاعات را کامل وارد کنید  ",
          textDirection: TextDirection.rtl,
        ),
      ));
    } else {
      String token = " ";
      String refreshToken = " ";
      String signinInfo = '{ ' +
          '"grant_type": "password",' +
          '"scope": "*",' +
          '"client_id": "MODRYPWOSWROHBLWZFXLFIRQATECZXQX",' +
          '"client_secret": "4020288305d1e3113a29da4049744980",' +
          '"username": "' +
          name +
          '",' +
          '"password": "' +
          pass +
          '"' +
          '}';
      //print("signinInfo = " +signinInfo);

      postSignInRequest(signinInfo).then((response) {
        Map<String, dynamic> jr = jsonDecode(response.body);
        if (jr.containsKey('access_token')) {
          token = jr['access_token'];
          refreshToken = jr['refresh_token'];
          print('token = ' + token);
          _fetchSessionID(token).then((sessionId) {
            setLoggedIn(name, token, sessionId, refreshToken)
                .then((bool commited) {
              showInSnackBar('وارد شدید');
              print('token = ' + token);
              Navigator.of(context).pushNamedAndRemoveUntil(
                  '/MainPage', (Route<dynamic> route) => false);
            });
          });
        } else if (jr.containsKey('error')) {
          if (jr['error'] == "invalid_grant") {
            showInSnackBar('اطلاعات وارد شده را کنترل کنید');
          }
        } else {}
      });
    }
  }

  Future<String> _fetchSessionID(String accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };
    String url =
        apiServer + 'extrarest/session-id';
    var response = await http.get(url, headers: requestHeaders);

    if (response.statusCode == 200) {
      String sessionId;

      var items = jsonDecode(response.body);

      //print('response.body = ' + response.body.toString());
      sessionId = items.toString();
      print('sessionId = ' + sessionId);
      return sessionId;
    } else {
      print('Exception');
      throw Exception('Failed to load internet');
    }
  }

  Future<http.Response> postSignInRequest(String signinInfo) async {
    // set up POST request arguments
    showInSnackBar('در حال ورود');
    String url = apiServer + 'oauth2/token';
    Map<String, String> headers = {"Content-type": "application/json"};
    String json = signinInfo;
    // make POST request
    http.Response response = http.Response('{}', 200);

    try {
      response = await http.post(url, headers: headers, body: json);
      return response;
    } catch (e) {
      print('failed');
      showInSnackBar('خطای ارتباط با سرور');
      return response;
    }
  }

  Future<bool> setLoggedIn(
      String name, String token, String sessionId, String refreshToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('name', name);
    prefs.setString('username', name);
    prefs.setString('access_token', token);
    prefs.setString('refresh_token', refreshToken);
    prefs.setString('session_id', sessionId);
    prefs.setBool('isLoggedIn', true);
    return true;
  }

  void signUp() {}
}
