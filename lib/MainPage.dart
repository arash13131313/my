import 'dart:convert';

import 'package:my/Darwer.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my/utils/AnimationHandler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_animations/simple_animations.dart';
import 'AppExpansionTile.dart';
import 'DashboardItem.dart';
import 'GlobalData.dart';
import 'MainPageItems.dart';
import 'package:http/http.dart' as http;
import 'NewTaskItem.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int currentItemsIndex = 0;
  final GlobalKey<AppExpansionTileState> expansionTileBox = GlobalKey();
  final GlobalKey<AppExpansionTileState> expansionTileNewTask = GlobalKey();
  bool isActiveInbox = true;
  bool isActiveSent = false;
  bool isActiveDraft = false;
  bool isActiveStoped = false;
  bool isBoxExpanded;
  bool iNewTAskExpanded;
  bool isNewTaskExpanded = false;
  bool _isInternetOff = false;
  List<NewTaskItem> newTaskItems;
  List<DashboardItem> dashboardItems;

  void callback(int currentItemsNum) {
    setState(() {
      currentItemsIndex = currentItemsNum;
    });
  }

  @override
  void initState() {
    super.initState();
    currentItemsIndex = 0;
    isBoxExpanded = true;
    isNewTaskExpanded = true;
    _isInternetOff = false;
    _isItemsLoaded = false;
    _getData().then((onValue) {
      setState(() {
        _isItemsLoaded = true;
      });
    });
  }

  bool _isItemsLoaded;

  Future _getData() async {
    String accessToken = await getAccessToken();
    print('access_token 1111= ' + accessToken);
    bool isValid = await _checkToken(accessToken);
    accessToken = await getAccessToken();
    print('isValid = ' + isValid.toString());
    newTaskItems = await _fetchNewTaskItems(accessToken);
    dashboardItems = await _fetchDashboardItems(accessToken);
  }

  Future<bool> _checkToken(accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };

    String url = apiServer + 'case/start-cases';
    try {
      var response = await http.get(url, headers: requestHeaders);
      if (response.statusCode == 200) {
        print('token checked');
        return true;
      } else if (response.statusCode == 401) {
        print('Unauthorized Check Token');
        bool isRefreshed = await _getRefreshToken(context);
        return isRefreshed;
      } else {
        print('Exception');
        throw Exception('Exception');
      }
    } catch (e) {
      setState(() {
        _isInternetOff = true;
      });
      throw Exception(e.toString());
    }
  }

  void _popUpMenuClicked(int id, BuildContext context) {
    if (id == 1) {
      Navigator.of(context).pushNamed('/UserPage');
    } else if (id == 2) {
      Navigator.of(context).pushNamed('/UserChangePassPage');
    } else {
      _logout(context);
    }
  }

  Future<bool> _getRefreshToken(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String refreshToken = prefs.getString('refresh_token') == null
        ? ' '
        : prefs.getString('refresh_token');
    if (refreshToken == ' ') {
      _setLogout(context);
      return false;
    } else {
      String refreshInfo = '{ ' +
          '"grant_type": "refresh_token",' +
          '"client_id": "MODRYPWOSWROHBLWZFXLFIRQATECZXQX",' +
          '"client_secret": "4020288305d1e3113a29da4049744980",' +
          '"refresh_token": "' +
          refreshToken +
          '"' +
          '}';
      String url = apiServer + 'oauth2/token';
      Map<String, String> headers = {"Content-type": "application/json"};
      String json = refreshInfo;
      // make POST request
      http.Response response = http.Response('{}', 200);

      try {
        response = await http.post(url, headers: headers, body: json);
        print('resfresh token response body' +
            response.body +
            ' ' +
            response.statusCode.toString());
        if (response.statusCode == 200) {
          Map<String, dynamic> jr = jsonDecode(response.body);
          await _setLoggedIn(prefs.getString('username'), jr['access_token'],
              jr['refresh_token']);

          return true;
        } else {
          _setLogout(context);
          return false;
        }
      } catch (e) {
        print('failed');
        throw Exception('failed');
      }
    }
  }

  Future<bool> _setLoggedIn(
      String name, String token, String refreshToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('name', name);
    prefs.setString('access_token', token);
    prefs.setString('refresh_token', refreshToken);
    prefs.setBool('isLoggedIn', true);
    return true;
  }

  Future<List<NewTaskItem>> _fetchNewTaskItems(accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };

    String url = apiServer + 'case/start-cases';
    //print('requestHeadersNewTAsks = ' + requestHeaders.toString());
    try {
      var response = await http.get(url, headers: requestHeaders);
      //print('responsebody = ' + response.body);
      if (response.statusCode == 200) {
        //print('response body = ' + response.body);
        final items = jsonDecode(response.body).cast<Map<String, dynamic>>();
        List<NewTaskItem> listOfNewTaskItems = items.map<NewTaskItem>((json) {
          return NewTaskItem.fromJson(json);
        }).toList();

        return listOfNewTaskItems;
      } else if (response.statusCode == 401) {
        print('Unauthorized Start Cases');
      } else {
        print('Exception');
        throw Exception('Exception');
      }
      return List();
    } catch (e) {
      setState(() {
        _isInternetOff = true;
      });

      throw Exception(e);
    }
  }

  Future<List<DashboardItem>> _fetchDashboardItems(accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };

    String url = apiServer + 'mobiledashboard/dashboard-list/0';
    //print('requestHeadersNewTAsks = ' + requestHeaders.toString());
    var response = await http.get(url, headers: requestHeaders);
    //print('responsebody dashboard-list = ' + response.body);
    if (response.statusCode == 200) {
      //print('response body dashboard-list = ' + response.body);
      final items = jsonDecode(response.body).cast<Map<String, dynamic>>();
      List<DashboardItem> listOfDashboardItems =
          items.map<DashboardItem>((json) {
        return DashboardItem.fromJson(json);
      }).toList();
      //print('listOfDashboardItems[listOfDashboardItems.length-1].id = ' + listOfDashboardItems[listOfDashboardItems.length-1].id);
      return listOfDashboardItems;
    } else if (response.statusCode == 401) {
      print('Unauthorized Dashboard LIst');
    } else {
      print('Exception');
      throw Exception('Exception');
    }
    return List();
  }

  double topBarOpacity = 0.0;

  void callbackScrollOffset(double offset) {
    if (offset >= 24) {
      if (topBarOpacity != 1.0) {
        setState(() {
          topBarOpacity = 1.0;
        });
      }
    } else if (offset <= 24 && offset >= 0) {
      if (topBarOpacity != offset / 24) {
        setState(() {
          topBarOpacity = offset / 24;
        });
      }
    } else if (offset <= 0) {
      if (topBarOpacity != 0.0) {
        setState(() {
          topBarOpacity = 0.0;
        });
      }
    }
  }

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var drawer2 = Drawer(
      child: DarwerPage(
        taskItems: newTaskItems,
        dashboardItems: dashboardItems,
        callback: callback,
        active: currentItemsIndex,
      ),
    );

    var appBar2 = AppBar(
      title: Row(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                top: 7 + 5 * topBarOpacity,
                bottom: 7 + 5 * topBarOpacity,
                left: 2 + topBarOpacity,
                right: 2 + topBarOpacity),
            child: Hero(
              tag: 'arianLogo',
              child: Image(
                image: AssetImage('images/login_icon.png'),
                color: _getColorAppBarWidgets(),
              ),
            ),
          ),
          Text(
            'آرین نوین رایانه',
            style: TextStyle(
                fontSize: textFontSizeTitle + 3 - 3 * topBarOpacity,
                fontWeight: FontWeight.bold,
                color: _getColorAppBarWidgets()),
          ),
        ],
      ),
      leading: IconButton(
        icon: Icon(
          Icons.menu,
          size: 32 - 9 * topBarOpacity,
        ),
        onPressed: () {
          _scaffoldKey.currentState.openDrawer();
        },
      ),
      iconTheme: new IconThemeData(
        color: _getColorAppBarWidgets(),
        size: 0.0,
      ),
      bottom: PreferredSize(
        child: Container(),
        preferredSize: Size(0, 12 - 12 * topBarOpacity),
      ),
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(32))),
      actions: <Widget>[
        PopupMenuButton<int>(
          icon: Icon(
            Icons.more_vert,
            size: 32 - 9 * topBarOpacity,
          ),
          color: colorPrimary.withOpacity(0.8),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          onSelected: (value) {
            _popUpMenuClicked(value, context);
          },
          itemBuilder: (BuildContext context) {
            return [
              PopupMenuItem(
                value: 1,
                child: InkWell(
                  child: Row(
                    children: <Widget>[
                      Icon(
                        FontAwesomeIcons.edit,
                        size: 16,
                        color: colorTextWhite,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Hero(
                        tag: 'editProfile',
                        child: Text(
                          'ویرایش پروفایل',
                          style: TextStyle(color: colorTextWhite),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              PopupMenuItem(
                value: 2,
                child: Row(
                  children: <Widget>[
                    Icon(
                      FontAwesomeIcons.userLock,
                      size: 16,
                      color: colorTextWhite,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Hero(
                        tag: 'changePass',
                        child: Text(
                          'تغییر رمز',
                          style: TextStyle(color: colorTextWhite),
                        ))
                  ],
                ),
              ),
              PopupMenuItem(
                value: 3,
                child: Row(
                  children: <Widget>[
                    Icon(
                      FontAwesomeIcons.doorOpen,
                      size: 16,
                      color: colorTextWhite,
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Text(
                      'خروج',
                      style: TextStyle(color: colorTextWhite),
                    )
                  ],
                ),
              ),
            ];
          },
        )
      ],
      centerTitle: true,
      backgroundColor: colorAccent.withOpacity(topBarOpacity),
      elevation: 0,
      flexibleSpace: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(32)),
          color: colorAccent.withOpacity(topBarOpacity),
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.grey.withOpacity(0.8 * topBarOpacity),
                offset: Offset(1.1, 1.1),
                blurRadius: 10.0),
          ],
        ),
      ),
    );
    //print('topBAr opacity = $topBarOpacity');
    var scaffold = Scaffold(
      key: _scaffoldKey,
      appBar: appBar2,
      drawer: drawer2,
      backgroundColor: colorBackground,
      body: Stack(
        children: <Widget>[
          Align(
            alignment: Alignment.bottomLeft,
            child: Container(
              height: 250,
              width: 250,
              decoration: BoxDecoration(
                color: Colors.blueGrey[50],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(0.0),
                    bottomLeft: Radius.circular(0.0),
                    bottomRight: Radius.circular(0.0),
                    topRight: Radius.circular(1000.0)),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: Container(
              height: 200,
              width: 95,
              decoration: BoxDecoration(
                color: Colors.blueGrey[50],
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(1000.0),
                    bottomLeft: Radius.circular(1000.0),
                    bottomRight: Radius.circular(0.0),
                    topRight: Radius.circular(0.0)),
              ),
            ),
          ),
          Container(
            child: _isItemsLoaded
                ? MainPageItems(
                    currentItemsIndex: currentItemsIndex,
                    callbackScrollOffset: callbackScrollOffset,
                  )
                : Center(
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        _isInternetOff
                            ? Column(
                                children: <Widget>[
                                  Text(
                                    'لطفا اتصال به اینترنت را کنترل کنید',
                                    style: TextStyle(
                                        color: colorTextPrimary,
                                        fontSize: textFontSizeTitle),
                                  ),
                                  SizedBox(
                                    height: 6,
                                  ),
                                  RaisedButton(
                                    child: Text('تلاش مجدد'),
                                    onPressed: () {
                                      setState(() {
                                        currentItemsIndex = 0;
                                        isBoxExpanded = true;
                                        isNewTaskExpanded = true;
                                        _isItemsLoaded = false;
                                        _isInternetOff = false;
                                        _getData().then((onValue) {
                                          setState(() {
                                            _isItemsLoaded = true;
                                          });
                                        });
                                      });
                                    },
                                    color: colorAccent,
                                    textColor: colorTextWhite,
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10)),
                                  ),
                                ],
                              )
                            : CircularProgressIndicator(),
                      ],
                    ),
                  ),
          ),
        ],
      ),
    );
    return scaffold;
  }

  void _logout(BuildContext mainContext) {
    showDialog(
      barrierDismissible: true,
      context: mainContext,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AnimationHandler().translateFromLeft(
            AlertDialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15),
              ),
              title: Text("قصد خروج دارید؟"),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                FlatButton(
                  child: Text("خیر"),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text("بله"),
                  onPressed: () {
                    _setLogout(mainContext);
                  },
                ),
              ],
            ),
            Curves.elasticOut,
            0);
      },
    );
  }

  Future<bool> setLoggedInToFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('name', ' ');
    prefs.setString('username', ' ');
    prefs.setString('token', ' ');
    prefs.setString('access_token', ' ');
    prefs.setString('refresh_token', ' ');
    prefs.setBool('isLoggedIn', false);
    return true;
  }

  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }

  void _setLogout(BuildContext mainContext) {
    setLoggedInToFalse().then((commited) {
      Navigator.of(mainContext).pushNamedAndRemoveUntil(
          '/LoginPage', (Route<dynamic> route) => false);
    });
  }

  int _getBlueOpacity() {
    int o = ((topBarOpacity) * 255).toInt();
    return o;
  }

  int _getGreenOpacity() {
    int o = ((topBarOpacity) * 255).toInt();
    return o;
  }

  int _getRedOpacity() {
    int o = ((topBarOpacity) * 255).toInt();
    return o;
  }

  int _getAlphaOpacity() {
    int o = ((topBarOpacity) * 255).toInt();
    o = colorTextTitle.alpha;
    o = ((255 - o) * topBarOpacity + o).toInt();
    return o;
  }

  Color _getColorAppBarWidgets() {
    Color color = colorTextTitle
        .withBlue(_getBlueOpacity())
        .withGreen(_getGreenOpacity())
        .withRed(_getRedOpacity())
        .withAlpha(_getAlphaOpacity());

    return color;
  }
}

//Animation
class FadeIn extends StatelessWidget {
  final double delay;
  final Widget child;

  FadeIn(this.delay, this.child);

  @override
  Widget build(BuildContext context) {
    final tween = MultiTrackTween([
      Track("opacity")
          .add(Duration(milliseconds: 500), Tween(begin: 0.0, end: 1.0)),
      Track("translateX").add(
          Duration(milliseconds: 500), Tween(begin: 130.0, end: 0.0),
          curve: Curves.easeOut)
    ]);

    return ControlledAnimation(
      delay: Duration(milliseconds: (300 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builderWithChild: (context, child, animation) => Opacity(
        opacity: animation["opacity"],
        child: Transform.translate(
            offset: Offset(animation["translateX"], 0), child: child),
      ),
    );
  }
}
