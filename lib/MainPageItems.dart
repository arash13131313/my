import 'dart:convert';

import 'package:my/GlobalData.dart';
import 'package:my/UserIframesPage.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_animations/simple_animations/controlled_animation.dart';
import 'package:simple_animations/simple_animations/multi_track_tween.dart';
import 'package:http/http.dart' as http;

import 'ProcessMapIframe.dart';

class MainPageItems extends StatefulWidget {
  final int currentItemsIndex;
  final Function callbackScrollOffset;
  const MainPageItems({
    this.currentItemsIndex,
    this.callbackScrollOffset,
  });

  @override
  _MainPageItemsState createState() => _MainPageItemsState();
}

class _MainPageItemsState extends State<MainPageItems> {
  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }

  Future<List<MainPageItem>> _fetchMainPageItemsInbox(
      accessToken, int currentItemsIndex) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };

    //print('currentItemsNum222 = ' + currentItemsIndex.toString());
    String url = apiServer + 'cases/inbox?limit=200';
    if (currentItemsIndex == 2) {
      url = apiServer + 'cases/draft?limit=200';
    } else if (currentItemsIndex == 3) {
      url = apiServer + 'cases/paused?limit=200';
    }

    //('accessTokenInbox = ' + accessToken);
    //print('urlInbox = ' + url);
    //print('requestHeaders = ' + requestHeaders.toString());
    var response = await http.get(url, headers: requestHeaders);
    //print('response.bodyInboxxx = ' + response.body);

    if (response.statusCode == 200) {
      final items = jsonDecode(response.body).cast<Map<String, dynamic>>();

      List<MainPageItem> listOfMainPageItems = items.map<MainPageItem>((json) {
        return MainPageItem.fromJson(json);
      }).toList();

      return listOfMainPageItems;
    } else {
      print('Exception');
      throw Exception('Failed to load internet');
    }
  }

  @override
  void initState() {
    super.initState();
    pr = new ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      isDismissible: true,
      showLogs: false,
    );
    pr.style(
        message: 'در حال بارگذاری ...',
        borderRadius: 10.0,
        backgroundColor: Colors.white,
        progressWidget: Container(
          child: CircularProgressIndicator(),
          padding: EdgeInsets.all(12),
        ),
        elevation: 10.0,
        insetAnimCurve: Curves.easeOutBack,
        progress: 0.0,
        maxProgress: 100.0,
        progressTextStyle: TextStyle(
            color: colorTextPrimary,
            fontSize: textFontSizeSplash,
            fontWeight: FontWeight.normal),
        messageTextStyle: TextStyle(
            color: colorTextPrimary,
            fontSize: textFontSizeTitle,
            fontWeight: FontWeight.normal));
    isInboxLoaded = false;
    currentIndex = widget.currentItemsIndex;
    _getMainData().then((onValue) {
      setState(() {});
    });
    scrollController.addListener(() {
      //print('scrollController.offset = ${scrollController.offset}');

      widget.callbackScrollOffset(scrollController.offset);
    });
  }

  Future _getMainData() async {
    String accessToken = await getAccessToken();
    print('access_token 1111= ' + accessToken);
    bool isValid = await _checkToken(accessToken);
    accessToken = await getAccessToken();
    print('isValid = ' + isValid.toString());

    _fetchMainPageItemsInbox(accessToken, widget.currentItemsIndex)
        .then((items) {
      setState(() {
        mainPageItems = items;
        isInboxLoaded = true;
      });
    });
  }

  Future<bool> _checkToken(accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };

    String url = apiServer + 'case/start-cases';
    var response = await http.get(url, headers: requestHeaders);
    if (response.statusCode == 200) {
      return true;
    } else if (response.statusCode == 401) {
      print('Unauthorized Check Token');
      bool isRefreshed = await _getRefreshToken(context);
      return isRefreshed;
    } else {
      print('Exception');
      throw Exception('Exception');
    }
  }

  void toggleProgressDialog(bool toggle) {
    if (!toggle) {
      if (pr.isShowing()) {
        pr.dismiss();
      }
    } else {
      if (!pr.isShowing()) {
        pr.show();
      }
    }
  }

  Future<bool> _getRefreshToken(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String refreshToken = prefs.getString('refresh_token') == null
        ? ' '
        : prefs.getString('refresh_token');
    if (refreshToken == ' ') {
      _setLogout(context);
      return false;
    } else {
      String refreshInfo = '{ ' +
          '"grant_type": "refresh_token",' +
          '"client_id": "MODRYPWOSWROHBLWZFXLFIRQATECZXQX",' +
          '"client_secret": "4020288305d1e3113a29da4049744980",' +
          '"refresh_token": "' +
          refreshToken +
          '"' +
          '}';
      String url = apiServer + 'oauth2/token';
      Map<String, String> headers = {"Content-type": "application/json"};
      String json = refreshInfo;
      // make POST request
      http.Response response = http.Response('{}', 200);

      try {
        response = await http.post(url, headers: headers, body: json);
        print('resfresh token response body' +
            response.body +
            ' ' +
            response.statusCode.toString());
        if (response.statusCode == 200) {
          Map<String, dynamic> jr = jsonDecode(response.body);
          await _setLoggedIn(prefs.getString('username'), jr['access_token'],
              jr['refresh_token']);

          return true;
        } else {
          _setLogout(context);
          return false;
        }
      } catch (e) {
        print('failed');
        throw Exception('failed');
      }
    }
  }

  Future<bool> _setLoggedIn(
      String name, String token, String refreshToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('name', name);
    prefs.setString('access_token', token);
    prefs.setString('refresh_token', refreshToken);
    prefs.setBool('isLoggedIn', true);
    return true;
  }

  Future<bool> setLoggedInToFalse() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('name', ' ');
    prefs.setString('username', ' ');
    prefs.setString('token', ' ');
    prefs.setString('access_token', ' ');
    prefs.setString('refresh_token', ' ');
    prefs.setBool('isLoggedIn', false);
    return true;
  }

  void _setLogout(BuildContext mainContext) {
    setLoggedInToFalse().then((commited) {
      Navigator.of(mainContext).pushNamedAndRemoveUntil(
          '/LoginPage', (Route<dynamic> route) => false);
    });
  }

  int currentIndex = 0;
  bool isInboxLoaded;
  List<MainPageItem> mainPageItems;
  ProgressDialog pr;
  var scrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    //print('currentItemsNum = ' + widget.currentItemsIndex.toString());
    //print('currentIndex == widget.currentItemsIndex ? ' + currentIndex.toString() + ' = ' + widget.currentItemsIndex.toString());
    if (currentIndex != widget.currentItemsIndex) {
      isInboxLoaded = false;
      getAccessToken().then((accessToken) {
        _fetchMainPageItemsInbox(accessToken, widget.currentItemsIndex)
            .then((items) {
          setState(() {
            mainPageItems = items;
            isInboxLoaded = true;
            currentIndex = widget.currentItemsIndex;
          });
        });
      });
    }
    return isInboxLoaded == null || !isInboxLoaded
        ? Center(child: CircularProgressIndicator())
        : NotificationListener<ScrollNotification>(
            onNotification: (scrollNotification) {
              if (scrollNotification is ScrollStartNotification) {
              } else if (scrollNotification is ScrollUpdateNotification) {
              } else if (scrollNotification is ScrollEndNotification) {
                _onScrollEnd(scrollNotification.metrics);
              }
              return true;
            },
            child: ListView.builder(
                controller: scrollController,
                itemCount: mainPageItems.length,
                scrollDirection: Axis.vertical,
                itemBuilder: (BuildContext context, int index) {
                  return MainPageItem(
                    title: mainPageItems[index].title == null
                        ? ' '
                        : mainPageItems[index].title,
                    sender: mainPageItems[index].sender == null
                        ? ' '
                        : mainPageItems[index].sender,
                    dateRecieved: mainPageItems[index].dateRecieved == null
                        ? ' '
                        : mainPageItems[index].dateRecieved,
                    processTitle: mainPageItems[index].processTitle == null
                        ? ' '
                        : mainPageItems[index].processTitle,
                    processDescription:
                        mainPageItems[index].processDescription == null
                            ? ' '
                            : mainPageItems[index].processDescription,
                    processCategory:
                        mainPageItems[index].processCategory == null
                            ? ' '
                            : mainPageItems[index].processCategory,
                    processsAuthor: mainPageItems[index].processsAuthor == null
                        ? ' '
                        : mainPageItems[index].processsAuthor,
                    processDateCreated:
                        mainPageItems[index].processDateCreated == null
                            ? ' '
                            : mainPageItems[index].processDateCreated,
                    caseTitle: mainPageItems[index].caseTitle == null
                        ? ' '
                        : mainPageItems[index].caseTitle,
                    caseName: mainPageItems[index].caseName == null
                        ? ' '
                        : mainPageItems[index].caseName,
                    caseDateRecieved:
                        mainPageItems[index].caseDateRecieved == null
                            ? ' '
                            : mainPageItems[index].caseDateRecieved,
                    caseDateStarted:
                        mainPageItems[index].caseDateStarted == null
                            ? ' '
                            : mainPageItems[index].caseDateStarted,
                    caseDateFinished:
                        mainPageItems[index].caseDateFinished == null
                            ? ' '
                            : mainPageItems[index].caseDateFinished,
                    taskTitle: mainPageItems[index].taskTitle == null
                        ? ' '
                        : mainPageItems[index].taskTitle,
                    taskDescription:
                        mainPageItems[index].taskDescription == null
                            ? ' '
                            : mainPageItems[index].taskDescription,
                    taskDateCreated:
                        mainPageItems[index].taskDateCreated == null
                            ? ' '
                            : mainPageItems[index].taskDateCreated,
                    taskDateFinishedExpectation:
                        mainPageItems[index].taskDateFinishedExpectation == null
                            ? ' '
                            : mainPageItems[index].taskDateFinishedExpectation,
                    taskDateFinished:
                        mainPageItems[index].taskDateFinished == null
                            ? ' '
                            : mainPageItems[index].taskDateFinished,
                    taskRunTime: mainPageItems[index].taskRunTime == null
                        ? ' '
                        : mainPageItems[index].taskRunTime,
                    appUID: mainPageItems[index].appUID == null
                        ? ' '
                        : mainPageItems[index].appUID,
                    proUID: mainPageItems[index].proUID == null
                        ? ' '
                        : mainPageItems[index].proUID,
                    taskUID: mainPageItems[index].taskUID == null
                        ? ' '
                        : mainPageItems[index].taskUID,
                    delIndex: mainPageItems[index].delIndex == null
                        ? '1'
                        : mainPageItems[index].delIndex,
                    progressDialogToggle: toggleProgressDialog,
                  );
                }),
          );
  }

  void _onScrollEnd(ScrollMetrics metrics) {
    print('metrics.pixels = ${metrics.pixels}');
    if (metrics.pixels < 24) {
      // scrollController.animateTo(0,
      //     duration: Duration(milliseconds: 200),
      //     curve: Curves.linear);
      //scrollController.jumpTo(200);
    }
  }
}

class MainPageItem extends StatefulWidget {
  final String title;
  final String sender;
  final String dateRecieved;
  final String processTitle;
  final String processDescription;
  final String processCategory;
  final String processsAuthor;
  final String processDateCreated;
  final String caseTitle;
  final String caseName;
  final String caseDateRecieved;
  final String caseDateStarted;
  final String caseDateFinished;
  final String taskTitle;
  final String taskDescription;
  final String taskDateCreated;
  final String taskDateFinishedExpectation;
  final String taskDateFinished;
  final String taskRunTime;
  final String delIndex;

  final String appUID;
  final String proUID;
  final String taskUID;

  final Function progressDialogToggle;

  MainPageItem({
    this.title,
    this.sender,
    this.dateRecieved,
    this.processTitle,
    this.processDescription,
    this.processCategory,
    this.processsAuthor,
    this.processDateCreated,
    this.caseTitle,
    this.caseName,
    this.caseDateRecieved,
    this.caseDateStarted,
    this.caseDateFinished,
    this.taskTitle,
    this.taskDescription,
    this.taskDateCreated,
    this.taskDateFinishedExpectation,
    this.taskDateFinished,
    this.taskRunTime,
    this.appUID,
    this.proUID,
    this.taskUID,
    this.delIndex,
    this.progressDialogToggle,
  });
  factory MainPageItem.fromJson(Map<String, dynamic> parsedJson) {
    return MainPageItem(
      title: parsedJson['app_pro_title'] ?? ' ',
      sender: parsedJson['app_init_usr_username'] ?? ' ',
      dateRecieved: parsedJson['app_update_date'] ?? ' ',
      processTitle: parsedJson['PRO_TITLE'] ?? ' ',
      processDescription: parsedJson['PRO_DESCRIPTION'] ?? ' ',
      processCategory: parsedJson['PRO_CATEGORY_LABEL'] ?? ' ',
      processsAuthor: parsedJson['PRO_AUTHOR'] ?? ' ',
      processDateCreated: parsedJson['PRO_CREATE_DATE'] ?? ' ',
      caseTitle: parsedJson['tas_title'] ?? ' ',
      caseName: (parsedJson['usr_firstname'] ?? ' ') +
          ' ' +
          (parsedJson['usr_lastname'] ?? ' '),
      caseDateRecieved: parsedJson['app_update_date'] ?? ' ',
      caseDateStarted: parsedJson['del_init_date'] ?? ' ',
      caseDateFinished: parsedJson['del_finish_date'] ?? ' ',
      taskTitle: parsedJson['taskTitle'] ?? ' ',
      taskDescription: parsedJson['taskDescription'] ?? ' ',
      taskDateCreated: parsedJson['taskDateCreated'] ?? ' ',
      taskDateFinishedExpectation:
          parsedJson['taskDateFinishedExpectation'] ?? ' ',
      taskDateFinished: parsedJson['taskDateFinished'] ?? ' ',
      taskRunTime: parsedJson['taskRunTime'] ?? ' ',
      appUID: parsedJson['app_uid'] ?? ' ',
      proUID: parsedJson['pro_uid'] ?? ' ',
      taskUID: parsedJson['tas_uid'] ?? ' ',
      delIndex: parsedJson['del_index'] ?? ' ',
    );
  }
  @override
  _MainPageItemState createState() => _MainPageItemState();
}

class _MainPageItemState extends State<MainPageItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var midRow = Container(
      padding: EdgeInsets.only(left: 8, right: 8, top: 4, bottom: 0),
      child: Row(
        children: <Widget>[
          Container(
            child: Icon(
              Icons.person,
              size: 15,
              color: colorTextSub,
            ),
          ),
          SizedBox(
            width: 2,
          ),
          Container(
            child: Text(
              widget.sender,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ],
      ),
    );
    var topRow = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Expanded(
          child: Container(
            padding: EdgeInsets.only(top: 8, right: 8),
            child: Text(
              widget.title,
              style: TextStyle(
                color: colorTextPrimary,
                fontWeight: FontWeight.bold,
                fontSize: textFontSizeTitle,
              ),
            ),
          ),
        ),
      ],
    );
    var botRow = Row(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(right: 8),
          child: Text(
            convertToJalali(widget.dateRecieved),
            style: TextStyle(color: colorTextSub),
            maxLines: 2,
          ),
        ),
        Expanded(
          child: Container(),
        ),
        InkWell(
          onTap: () {
            showProcessMap();
          },
          child: Container(
            padding: EdgeInsets.only(top: 8, right: 12, left: 12, bottom: 12),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.map,
                  color: colorFlatButton,
                  size: 18,
                ),
                Container(
                  padding: EdgeInsets.only(right: 2),
                  child: Text(
                    'مشاهده نقشه فرآیند',
                    style: TextStyle(color: colorFlatButton),
                  ),
                ),
              ],
            ),
          ),
        ),
        InkWell(
          onTap: () {
            showDetailsDialog(context);
          },
          child: Container(
            padding: EdgeInsets.only(top: 8, right: 12, left: 12, bottom: 12),
            child: Row(
              children: <Widget>[
                Icon(
                  Icons.info_outline,
                  color: colorFlatButton,
                  size: 18,
                ),
                Container(
                  padding: EdgeInsets.only(right: 2),
                  child: Text(
                    'جزئیات',
                    style: TextStyle(color: colorFlatButton),
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    );
    var card = Container(
      margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
      // padding: EdgeInsets.only(top: 8, bottom: 1),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(32.0),
            bottomLeft: Radius.circular(6.0),
            bottomRight: Radius.circular(6.0),
            topRight: Radius.circular(6.0)),
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              offset: Offset(1.1, 1.1),
              blurRadius: 10.0),
        ],
      ),
      child: InkWell(
        onTap: () {
          onCardClicked(context);
        },
        child: Stack(
          overflow: Overflow.clip,
          //alignment: Alignment.topLeft,
          children: <Widget>[
            Container(
              //color: Colors.green,
              padding: EdgeInsets.only(top: 8, right: 8, left: 8, bottom: 5),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  //top row
                  FadeIn(0.66, topRow),
                  //mid row
                  FadeIn(1.0, midRow),
                  //bot row
                  FadeIn(1.0, botRow),
                ],
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: 75,
                height: 60,
                //margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                //padding: EdgeInsets.only(top: 8, bottom: 1),
                decoration: BoxDecoration(
                  color: Colors.lightBlue.withOpacity(0.02),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(53.0),
                      bottomLeft: Radius.circular(0.0),
                      bottomRight: Radius.circular(100.0),
                      topRight: Radius.circular(0.0)),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: 50,
                height: 40,
                //margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                decoration: BoxDecoration(
                  color: Colors.lightBlue.withOpacity(0.02),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(79.0),
                      bottomLeft: Radius.circular(0.0),
                      bottomRight: Radius.circular(100.0),
                      topRight: Radius.circular(0.0)),
                ),
              ),
            ),
            Positioned(
              top: 80,
              right: 115,
              child: Container(
                width: 75,
                height: 60,
                //margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                //padding: EdgeInsets.only(top: 8, bottom: 1),
                decoration: BoxDecoration(
                  color: Colors.lightBlue.withOpacity(0.02),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(100.0),
                      bottomLeft: Radius.circular(0.0),
                      bottomRight: Radius.circular(0.0),
                      topRight: Radius.circular(100.0)),
                ),
              ),
            ),
            Positioned(
              top: 100,
              right: 131,
              child: Container(
                width: 40,
                height: 40,
                //margin: EdgeInsets.symmetric(vertical: 8, horizontal: 8),
                //padding: EdgeInsets.only(top: 8, bottom: 1),
                decoration: BoxDecoration(
                  color: Colors.lightBlue.withOpacity(0.02),
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(100.0),
                      bottomLeft: Radius.circular(0.0),
                      bottomRight: Radius.circular(0.0),
                      topRight: Radius.circular(100.0)),
                ),
              ),
            ),
          ],
        ),
      ),
    );
    return Padding(
      padding: const EdgeInsets.only(top: 0, bottom: 0, left: 8.0, right: 8.0),
      child: FadeIn(0.33, card),
    );
  }

  Future<MainPageItem> _fetchMainPageItemsProccessInfo(String proUID) async {
    print('proUID = ' + proUID.toString());
    String url = apiServer + 'processinfo/processinformation?ProUid=' + proUID;
    widget.progressDialogToggle(true);
    try {
      var response = await http.get(url);

      if (response.statusCode == 200) {
        var items = jsonDecode(response.body);
        MainPageItem mainPageItem = MainPageItem(
          processTitle: items['PRO_TITLE'] == null ? ' ' : items['PRO_TITLE'],
          processDescription:
              items['PRO_DESCRIPTION'] == null ? ' ' : items['PRO_DESCRIPTION'],
          processCategory: items['PRO_CATEGORY_LABEL'] == null
              ? ' '
              : items['PRO_CATEGORY_LABEL'],
          processsAuthor:
              items['PRO_AUTHOR'] == null ? ' ' : items['PRO_AUTHOR'],
          processDateCreated:
              items['PRO_CREATE_DATE'] == null ? ' ' : items['PRO_CREATE_DATE'],
        );

        return mainPageItem;
      } else {
        print('Exception');
        throw Exception('Failed to load internet');
      }
    } catch (e) {
      widget.progressDialogToggle(false);

      
      print('No Internet Process Info');
      throw Exception(e);
    }
  }

  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }

  Future<String> getSessionId() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String sessionId = prefs.getString('session_id') == null
        ? ' '
        : prefs.getString('session_id');
    return sessionId;
  }

  // Fetch Case Info
  Future<MainPageItem> _fetchMainPageItemsCaseInfo(
      String appUID, accessToken, String taskUID) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };
    print('proUID = ' + appUID.toString());
    String url = apiServer + 'cases/$appUID/tasks';
    print('url = ' + url);
    try {
      var response = await http.get(url, headers: requestHeaders);

      if (response.statusCode == 200) {
        var items = jsonDecode(response.body);

        int count = ((items as List).length);
        for (var i = 0; i < count; i++) {
          if (items[i]['tas_uid'] == taskUID) {
            print('usr_firstname = ' +
                items[i]['delegations'][0]['usr_firstname']);
            MainPageItem mainPageItem = MainPageItem(
              caseTitle:
                  items[i]['tas_title'] == null ? ' ' : items[i]['tas_title'],
              caseName: (items[i]['delegations'][0]['usr_firstname'] == null
                      ? ' '
                      : items[i]['delegations'][0]['usr_firstname']) +
                  ' ' +
                  (items[i]['delegations'][0]['usr_lastname'] == null
                      ? ' '
                      : items[i]['delegations'][0]['usr_lastname']),
              caseDateStarted:
                  items[i]['delegations'][0]['del_init_date'] == null
                      ? ' '
                      : items[i]['delegations'][0]['del_init_date'],
              caseDateFinished:
                  items[i]['delegations'][0]['del_finish_date'] == null
                      ? ' '
                      : items[i]['delegations'][0]['del_finish_date'],
            );

            return mainPageItem;
          }
        } //for
        MainPageItem mainPageItem = MainPageItem(
          caseTitle: ' ',
          caseName: ' ',
          caseDateStarted: ' ',
          caseDateFinished: ' ',
        );
        return mainPageItem;
      } else {
        print('Exception');
        throw Exception('Failed to load internet');
      }
    } catch (e) {
      widget.progressDialogToggle(false);
      
      print('No Internet Case Info');
      throw Exception('Failed to load internet');
    }
  }

  void showDetailsDialog(BuildContext context) {
    //print('showCertificateDialog');

    _fetchMainPageItemsProccessInfo(widget.proUID).then((m) {
      print('m.processTitle' + m.processTitle);

      var processInfo = ListView(
        shrinkWrap: true,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 4),
                  child: Text(
                    'عنوان',
                    style: TextStyle(color: colorPrimary),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                  child: Divider(),
                ),
                Text(
                  m.processTitle,
                  style: TextStyle(
                      color: colorTextSub,
                      fontWeight: FontWeight.bold,
                      fontSize: textFontSizeTitle - 1),
                ),
              ],
            ),
          ),
          Container(
            child: Divider(
              color: colorDivider,
              height: 2,
              thickness: 1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 4),
                  child: Text(
                    'توضیحات',
                    style: TextStyle(color: colorPrimary),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                  child: Divider(),
                ),
                Text(
                  m.processDescription,
                  style: TextStyle(
                      color: colorTextSub,
                      fontWeight: FontWeight.bold,
                      fontSize: textFontSizeTitle - 1),
                ),
              ],
            ),
          ),
          Container(
            child: Divider(
              color: colorDivider,
              height: 2,
              thickness: 1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 4),
                  child: Text(
                    'دسته بندی',
                    style: TextStyle(color: colorPrimary),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                  child: Divider(),
                ),
                Text(
                  m.processCategory,
                  style: TextStyle(
                      color: colorTextSub,
                      fontWeight: FontWeight.bold,
                      fontSize: textFontSizeTitle - 1),
                ),
              ],
            ),
          ),
          Container(
            child: Divider(
              color: colorDivider,
              height: 2,
              thickness: 1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 4),
                  child: Text(
                    'نویسنده',
                    style: TextStyle(color: colorPrimary),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                  child: Divider(),
                ),
                Text(
                  m.processsAuthor,
                  style: TextStyle(
                      color: colorTextSub,
                      fontWeight: FontWeight.bold,
                      fontSize: textFontSizeTitle - 1),
                ),
              ],
            ),
          ),
          Container(
            child: Divider(
              color: colorDivider,
              height: 2,
              thickness: 1,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(left: 4),
                  child: Text(
                    'تاریخ ایجاد',
                    style: TextStyle(color: colorPrimary),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                  child: Divider(),
                ),
                Text(
                  convertToJalali(m.processDateCreated),
                  style: TextStyle(
                      color: colorTextSub,
                      fontWeight: FontWeight.bold,
                      fontSize: textFontSizeTitle - 1),
                ),
              ],
            ),
          ),
        ],
      ); // here
      getAccessToken().then((accessToken) {
        _fetchMainPageItemsCaseInfo(widget.appUID, accessToken, widget.taskUID)
            .then((m2) {
          var caseInfo = ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'عنوان',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      m2.caseTitle,
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  color: colorDivider,
                  height: 2,
                  thickness: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'نام و نام خانوادگی',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      m2.caseName,
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  color: colorDivider,
                  height: 2,
                  thickness: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'تاریخ دریافت',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      convertToJalali(widget.caseDateRecieved),
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  color: colorDivider,
                  height: 2,
                  thickness: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'تاریخ شروع',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      convertToJalali(m2.caseDateStarted),
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  color: colorDivider,
                  height: 2,
                  thickness: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'تاریخ پایان',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      convertToJalali(m2.caseDateFinished),
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
            ],
          );

          var taskInfo = ListView(
            shrinkWrap: true,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'عنوان',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      widget.taskTitle,
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  color: colorDivider,
                  height: 2,
                  thickness: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'توضیحات',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      widget.taskDescription,
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  color: colorDivider,
                  height: 2,
                  thickness: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'تاریخ ایجاد',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      convertToJalali(widget.taskDateCreated),
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  color: colorDivider,
                  height: 2,
                  thickness: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'تاریخ انتظار اتمام',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      convertToJalali(widget.taskDateFinishedExpectation),
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  color: colorDivider,
                  height: 2,
                  thickness: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'تاریخ اتمام',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      convertToJalali(widget.taskDateFinished),
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
              Container(
                child: Divider(
                  color: colorDivider,
                  height: 2,
                  thickness: 1,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(left: 4),
                      child: Text(
                        'مدت زمان انجام',
                        style: TextStyle(color: colorPrimary),
                      ),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 70, right: 70, bottom: 4),
                      child: Divider(),
                    ),
                    Text(
                      convertToJalali(widget.taskDateFinished),
                      style: TextStyle(
                          color: colorTextSub,
                          fontWeight: FontWeight.bold,
                          fontSize: textFontSizeTitle - 1),
                    ),
                  ],
                ),
              ),
            ],
          );

          Dialog dialog = Dialog(
            //shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),

            backgroundColor: Colors.transparent,
            child: Container(
              height: 700.0,
              width: 400.0,
              padding: EdgeInsets.only(top: 0, left: 0, right: 0, bottom: 0),
              child: Card(
                clipBehavior: Clip.antiAlias,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0)),
                child: DefaultTabController(
                  length: 3,
                  child: Scaffold(
                    appBar: TabBar(
                      labelColor: colorPrimary,
                      unselectedLabelColor: colorTextPrimary,
                      indicatorColor: colorPrimary,
                      tabs: [
                        Tab(
                          text: 'اطلاعات فرآیند',
                        ),
                        Tab(
                          text: 'تاریخچه وظیفه',
                        ),
                        Tab(
                          text: 'مشخصات وظیفه',
                        ),
                      ],
                    ),
                    body: TabBarView(
                      children: [
                        processInfo,
                        caseInfo,
                        taskInfo,
                      ],
                    ),
                  ),
                ),
              ),
            ),
          );
          widget.progressDialogToggle(false);
          showDialog(
              context: context, builder: (BuildContext context) => dialog);
        });
      });
    });
  }

  void onCardClicked(BuildContext context) {
    print('onCardClicked');
    getSessionId().then((sessionId) {
      Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => UserIframesPage(
                  appUID: widget.appUID,
                  delIndex: widget.delIndex,
                  type: 0,
                  sessionId: sessionId,
                )),
      );
    });
  }

  void showProcessMap() {
    print('appUID = ' + widget.appUID + ' | processID = ' + widget.proUID);
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => ProcessMapIframe(
                appUid: widget.appUID,
                processId: widget.proUID,
              )),
    );
  }

  
}

//Animation

class FadeIn extends StatelessWidget {
  final double delay;
  final Widget child;

  FadeIn(this.delay, this.child);

  @override
  Widget build(BuildContext context) {
    final tween = MultiTrackTween([
      Track("opacity")
          .add(Duration(milliseconds: 300), Tween(begin: 0.0, end: 1.0)),
      Track("translateX").add(
          Duration(milliseconds: 300), Tween(begin: 100.0, end: 0.0),
          curve: Curves.easeOut)
    ]);

    return ControlledAnimation(
      delay: Duration(milliseconds: (300 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builderWithChild: (context, child, animation) => Opacity(
        opacity: animation["opacity"],
        child: Transform.translate(
            offset: Offset(animation["translateX"], 0), child: child),
      ),
    );
  }
}
