import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'GlobalData.dart';
import 'UserIframesPage.dart';

class NewTaskItem extends StatefulWidget {
  final String taskUid;
  final String proTitle;
  final String proUid;
  final String proCategory;
  final String categoryName;

  NewTaskItem({
    this.taskUid,
    this.proTitle,
    this.proUid,
    this.proCategory,
    this.categoryName,
  });
  factory NewTaskItem.fromJson(Map<String, dynamic> parsedJson) {
    return NewTaskItem(
      taskUid: parsedJson['tas_uid'],
      proTitle: parsedJson['pro_title'],
      proUid: parsedJson['pro_uid'],
      proCategory: parsedJson['pro_category'],
      categoryName: parsedJson['category_name'],
    );
  }
  @override
  _NewTaskItemState createState() => _NewTaskItemState();
}

class _NewTaskItemState extends State<NewTaskItem> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _onNewTaskClicked(context, widget.proUid, widget.taskUid);
      },
      child: Container(
        //width: 250,
        padding: EdgeInsets.only(right: 26, bottom: 4),
        alignment: Alignment.topRight,
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Icon(
                  Icons.arrow_right,
                  color: colorFlatButton,
                ),
                Flexible(
                  child: Text(
                    widget.proTitle,
                    style: TextStyle(
                        color: colorFlatButton,
                        fontWeight: FontWeight.normal,
                        fontSize: textFontSizeTitle),
                  ),
                ),
              ],
            ),
            Divider(indent: 16,endIndent: 32,)
          ],
        ),
      ),
    );
  }

  void _onNewTaskClicked(BuildContext context, String proUid, String taskUid) {
    print('proUid = ' + proUid);
    print('taskUid = ' + taskUid);
    String newTaskInfo = '{ ' +
        '"pro_uid": "' +
        proUid +
        '",' +
        '"tas_uid": "' +
        taskUid +
        '"' +
        '}';
    getAccessToken().then((accesToken) {
      _postCreateNewTask(newTaskInfo, accesToken).then((response) {
        //do stuff
        if (response.statusCode == 200) {
          Map<String, dynamic> jr = jsonDecode(response.body);
          String appUid = jr['app_uid'];
          

          _fetchSessionID(accesToken).then((sessionId) {
            print('sessionId = ' + sessionId);
            Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) => UserIframesPage(
                        appUID: appUid,
                        delIndex: '1',
                        type: 0,
                        sessionId: sessionId,
                      )),
            );
          });
        } else {
          throw Exception(response.statusCode.toString());
        }
      });
    });
  }

  Future<String> _fetchSessionID(String accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };
    String url =
        apiServer + 'extrarest/session-id';
    var response = await http.get(url, headers: requestHeaders);

    if (response.statusCode == 200) {
      String sessionId;

      var items = jsonDecode(response.body);

      //print('response.body = ' + response.body.toString());
      sessionId = items.toString();
      print('sessionId = ' + sessionId);
      return sessionId;
    } else {
      print('Exception');
      throw Exception('Failed to load internet');
    }
  }

  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }

  Future<http.Response> _postCreateNewTask(
      String newTaskInfo, String accessToken) async {
    // set up POST request arguments
    String url = apiServer + 'cases';
    Map<String, String> headers = {
      "Authorization": "Bearer $accessToken",
      "Content-type": "application/json"
    };
    String json = newTaskInfo;
    // make POST request
    http.Response response = http.Response('{}', 200);

    try {
      response = await http.post(url, headers: headers, body: json);
      return response;
    } catch (e) {
      throw Exception('failed');
    }
  }

  
}
