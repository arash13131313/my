import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'GlobalData.dart';

class ProcessMapIframe extends StatefulWidget {
  final String processId;
  final String appUid;

  ProcessMapIframe({
    this.processId,
    this.appUid,
  });
  @override
  _ProcessMapIframeState createState() => _ProcessMapIframeState();
}

class _ProcessMapIframeState extends State<ProcessMapIframe> {
  String sessionId;
  bool isLoaded = false;
  @override
  void initState() {
    super.initState();
    getAccessToken().then((accessToken) {
      _fetchSessionID(accessToken).then((sid) {
        setState(() {
          sessionId = sid;
          link =
              iframeServer + 'designer?prj_uid=' +
                  widget.processId +
                  '&prj_readonly=true&app_uid=' +
                  widget.appUid +
                  '&sid=' +
                  sessionId;
          print('linkToMap = $link');
          isLoaded = true;
        });
      });
    });
  }

  String link;
  @override
  Widget build(BuildContext context) {
    return isLoaded
        ? WebviewScaffold(
            url: link,
            appBar: new AppBar(
              title: new Text("آرین نوین رایانه"),
              backgroundColor: colorAccent,
            ),
            displayZoomControls: false,
            withJavascript: true,
            hidden: true,
            withZoom: true,
          )
        : Scaffold(
            appBar: new AppBar(
              title: new Text("آرین نوین رایانه"),
              backgroundColor: colorAccent,
            ),
            body: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }

  Future<String> _fetchSessionID(String accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };
    String url =
        apiServer + 'extrarest/session-id';
    var response = await http.get(url, headers: requestHeaders);

    if (response.statusCode == 200) {
      String sessionId;

      var items = jsonDecode(response.body);

      //print('response.body = ' + response.body.toString());
      sessionId = items.toString();
      print('sessionId = ' + sessionId);
      return sessionId;
    } else {
      print('Exception');
      throw Exception('Failed to load internet');
    }
  }

  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }
}
