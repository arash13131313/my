import 'dart:async';
import 'package:flutter/material.dart';
import 'package:my/MainPage.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'GlobalData.dart';
import 'Login.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  bool _isLoggedIn = false;

  @override
  void initState() {
    super.initState();
    getLoggedIn().then((isLoggedIn) {
      setState(() {
        _isLoggedIn = isLoggedIn;
      });
    });
    Timer(
      Duration(seconds: 2),
      () => Navigator.of(context).pushReplacement(
        PageRouteBuilder(
          transitionDuration: Duration(seconds: 1),
          pageBuilder: (_, __, ___) => _isLoggedIn
              ? MainPage()
              : Directionality(
                  textDirection: TextDirection.ltr,
                  child: new Builder(
                    builder: (BuildContext context) {
                      return new MediaQuery(
                        data: MediaQuery.of(context).copyWith(
                          textScaleFactor: 1.0,
                        ),
                        child: LoginPage(),
                      );
                    },
                  ),
                ),
        ),
      ),
    );
  }

  Future<bool> getLoggedIn() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool isLoggedIn = prefs.getBool('isLoggedIn') == null
        ? false
        : prefs.getBool('isLoggedIn');
    return isLoggedIn;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        decoration: BoxDecoration(
          gradient: RadialGradient(
            center: Alignment.topLeft,
            stops: [0.1, 0.3, 0.8],
            radius: 2,
            colors: splashGradient,
          ),
        ),
        alignment: Alignment.center,
        child: Center(
          child: Container(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Hero(
                  tag: 'arianLogo',
                  child: Image(
                    image: AssetImage('images/login_icon.png'),
                    color: Colors.white,
                    height: 150,
                    width: 150,
                  ),
                ),
                Text(
                  'آرین نوین رایانه',
                  style: TextStyle(
                    color: colorTextWhite,
                    fontSize: textFontSizeSplash,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
