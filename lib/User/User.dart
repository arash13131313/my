class User {
  final String username;
  final String firstName;
  final String lastName;
  final String email;
  final String country;
  final String city;
  final String address;
  final String phone;
  final String fax;
  final String cellular; //Mobile Phone
  final String zipCode;
  final String birthDay;
  final String image;
  final String accessToken;
  final String uID;

  User({
    this.username,
    this.firstName,
    this.lastName,
    this.email,
    this.country,
    this.city,
    this.address,
    this.phone,
    this.fax,
    this.cellular,
    this.zipCode,
    this.birthDay,
    this.image,
    this.accessToken,
    this.uID,
  });
}