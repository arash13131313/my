import 'package:my/MainPage.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../GlobalData.dart';
import 'User.dart';
import 'package:http/http.dart' as http;

class UserChangePassPage extends StatefulWidget {
  @override
  _UserChangePassPageState createState() => _UserChangePassPageState();
}

class _UserChangePassPageState extends State<UserChangePassPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isRight = false;
  bool passwordVisibleOld;
  bool passwordVisibleNew;
  bool passwordVisibleConfirmNew;

  User user = User();
  bool isStillLoading = true;
  TextEditingController _controllerOldPassword = TextEditingController();
  TextEditingController _controllerNewPassword = TextEditingController();
  TextEditingController _controllerConfirmNewPassword = TextEditingController();
  @override
  void initState() {
    super.initState();
    passwordVisibleOld = false;
    passwordVisibleNew = false;
    passwordVisibleConfirmNew = false;
    getUsername().then((username) {
      setState(() {
        user = User(username: username);
        isStillLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    Widget loading = Scaffold(
      appBar: AppBar(
        title: Hero(tag: 'editProfile', child: Text('تغییر رمز')),
        centerTitle: true,
        backgroundColor: colorAccent,
      ),
      body: Container(
        child: Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
    Widget loaded = isStillLoading
        ? loading
        : Scaffold(
            key: _scaffoldKey,
            appBar: AppBar(
              title: Text('تغییر رمز'),
              centerTitle: true,
              backgroundColor: colorAccent,
            ),
            body: Container(
              padding: EdgeInsets.symmetric(horizontal: 32),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'نام کاربری :',
                        style: TextStyle(color: colorTextPrimary),
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(
                        user.username,
                        style: TextStyle(
                          color: colorTextPrimary,
                          fontSize: textFontSizeTitle,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 50),
                    child: Divider(
                      color: colorTextSub,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 4, top: 8),
                    child: TextField(
                      controller: _controllerOldPassword,
                      obscureText: !passwordVisibleOld,
                      decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisibleOld
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Theme.of(context).primaryColorDark,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                passwordVisibleOld = !passwordVisibleOld;
                              });
                            },
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          isDense: true,
                          labelText: 'رمز عبور فعلی'),
                      style: TextStyle(
                        fontSize: textFontSizeTitle - 1,
                        color: colorTextPrimary,
                        height: 1,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 4, top: 8),
                    child: TextField(
                      controller: _controllerNewPassword,
                      obscureText: !passwordVisibleNew,
                      decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisibleNew
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Theme.of(context).primaryColorDark,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                passwordVisibleNew = !passwordVisibleNew;
                              });
                            },
                          ),
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(10)),
                          isDense: true,
                          labelText: 'رمز عبور جدید'),
                      style: TextStyle(
                        fontSize: textFontSizeTitle - 1,
                        color: colorTextPrimary,
                        height: 1,
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.only(left: 4, top: 8),
                    child: TextField(
                      controller: _controllerConfirmNewPassword,
                      onChanged: (pass) {
                        if (pass == _controllerNewPassword.text) {
                          setState(() {
                            isRight = true;
                          });
                        } else {
                          setState(() {
                            isRight = false;
                          });
                        }
                      },
                      obscureText: !passwordVisibleConfirmNew,
                      decoration: InputDecoration(
                          suffixIcon: IconButton(
                            icon: Icon(
                              // Based on passwordVisible state choose the icon
                              passwordVisibleConfirmNew
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              color: Theme.of(context).primaryColorDark,
                            ),
                            onPressed: () {
                              // Update the state i.e. toogle the state of passwordVisible variable
                              setState(() {
                                passwordVisibleConfirmNew =
                                    !passwordVisibleConfirmNew;
                              });
                            },
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                                color: isRight ? colorCorrect : colorWrong,
                                width: 0.0),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                                color: isRight ? colorCorrect : colorWrong,
                                width: 0.0),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10),
                            borderSide: BorderSide(
                                color: isRight ? colorCorrect : colorWrong,
                                width: 0.0),
                          ),
                          isDense: true,
                          labelStyle: TextStyle(
                              color: isRight ? colorCorrect : colorWrong),
                          labelText: 'تکرار رمز عبور جدید'),
                      style: TextStyle(
                        fontSize: textFontSizeTitle - 1,
                        color: isRight ? colorCorrect : colorWrong,
                        height: 1,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 8,
                  ),
                  RaisedButton(
                    padding: EdgeInsets.symmetric(vertical: 12, horizontal: 32),
                    onPressed: () {
                      if (_controllerConfirmNewPassword.text ==
                          _controllerNewPassword.text) {
                        submitUserEdit();
                        setState(() {
                          isRight = true;
                        });
                      } else {
                        setState(() {
                          isRight = false;
                        });
                      }
                    },
                    child: Text('تغییر رمز'),
                    color: colorPrimary,
                    textColor: colorTextWhite,
                  )
                ],
              ),
            ),
          );

    return isStillLoading ? loading : loaded;
  }

  void submitUserEdit() {
    getAccessToken().then((accessToken) {
      String userInfo = '{ ' +
          '"username": "' +
          user.username +
          '",' +
          '"old_password": "' +
          _controllerOldPassword.text +
          '",' +
          '"usr_cnf_pass": "' +
          _controllerConfirmNewPassword.text +
          '",' +
          '"usr_new_pass": "' +
          _controllerNewPassword.text +
          '"' +
          '}';
      _putEditUser(userInfo, accessToken).then((response) {
        print('responseChange Pass = ' + response.body);
        if (response.statusCode == 200) {
          if (response.body == 'رمز فعلی صحیح نیست' ||
              response.body == 'رمز عبور و تکرار آن یکسان نیستند') {
            showInSnackBar(response.body);
          } else {
            showInSnackBar('تغییر رمز عبور اعمال شد');
            Future.delayed(const Duration(milliseconds: 2000), () {
              Navigator.of(context).pushReplacement(MaterialPageRoute(
                  builder: (BuildContext context) => MainPage()));
            });
          }
        } else if (response.statusCode == 400) {
          showInSnackBar('لطفا اطلاعات وارد شده را کنترل کنید');
        } else {
          showInSnackBar('خطا در اتصال به اینترنت');
        }
      });
    });
  }

  Future<http.Response> _putEditUser(
      String userInfo, String accessToken) async {
    // set up POST request arguments
    String url =
        apiServer + 'user/change-password' /*+ uid*/;
    Map<String, String> requestHeaders = {
      'Authorization': 'bearer $accessToken',
      "Content-type": "application/json"
    };
    String json = userInfo;
    // make POST request

    try {
      http.Response response =
          await http.put(url, headers: requestHeaders, body: json);
      return response;
    } catch (e) {
      print('failed');
      throw Exception('failed');
    }
  }

  void showInSnackBar(String value) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.center,
        style:
            TextStyle(color: colorTextWhite, fontSize: textFontSizeTitle - 1, fontFamily: "Sans"),
      ),
      backgroundColor: colorSecond,
      duration: Duration(seconds: 3),
    ));
  }

  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }

  Future<String> getUsername() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String username =
        prefs.getString('name') == null ? ' ' : prefs.getString('name');
    return username;
  }
}
