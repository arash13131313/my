import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_dash/flutter_dash.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:persian_datepicker/persian_datepicker.dart';
import 'package:shamsi_date/shamsi_date.dart';

import '../GlobalData.dart';
import 'User.dart';

class UserPage extends StatefulWidget {
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  //TextField Controllers
  TextEditingController _controllerFirstName = TextEditingController();
  TextEditingController _controllerLastName = TextEditingController();
  TextEditingController _controllerUsername = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerCountry = TextEditingController();
  TextEditingController _controllerCity = TextEditingController();
  TextEditingController _controllerAddress = TextEditingController();
  TextEditingController _controllerPhone = TextEditingController();
  TextEditingController _controllerFax = TextEditingController();
  TextEditingController _controllerCellular = TextEditingController();
  TextEditingController _controllerZipCode = TextEditingController();
  TextEditingController _controllerBirthDay = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();
  PersianDatePickerWidget persianDatePicker;
  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }

  Future<User> _fetchUserDetail(String accessToken) async {
    String urlUID = apiServer + 'userextend/get-my-uid';
    Map<String, String> requestHeadersUID = {
      'Authorization': 'Bearer $accessToken'
    };
    var responseUID = await http.get(urlUID, headers: requestHeadersUID);

    if (responseUID.statusCode == 200) {
      String item = responseUID.body;

      item = item.substring(1, item.length - 1);

      String userUID = item;

      String url = apiServer + 'user/' + userUID;

      print('url = ' + url);
      print('accessToken = ' + accessToken);
      Map<String, String> requestHeaders = {
        'Authorization': 'Bearer $accessToken'
      };
      var response = await http.get(url, headers: requestHeaders);
      print('response = ' + response.body);
      if (response.statusCode == 200) {
        final item = jsonDecode(response.body);
        //print('item = ' + item.toString());
        //print('userName = ' + item['usr_username']);
        User user = User(
          username: item['usr_username'],
          firstName: item['usr_firstname'],
          lastName: item['usr_lastname'],
          email: item['usr_email'],
          country: item['usr_country'],
          city: item['usr_city'],
          address: item['usr_address'],
          phone: item['usr_phone'],
          fax: item['usr_fax'],
          cellular: item['usr_cellular'],
          zipCode: item['usr_zip_code'],
          birthDay: item['usr_birthday'],
          image: item['usr_photo_path'],
          uID: userUID,
        );

        return user;
      } else if (response.statusCode == 401) {
        print('Unathorized');
        return user;
      } else {
        print('Exception');
        throw Exception('Failed to load internet');
      }
    } else {
      print('Exception');
      throw Exception('Failed to load internet');
    }
  }

  User user = User();
  bool isStillLoading = true;
  @override
  void initState() {
    super.initState();

    getAccessToken().then((accessToken) {
      _fetchUserDetail(accessToken).then((u) {
        setState(() {
          user = u;
          isStillLoading = false;
          _controllerUsername.text = user.username ?? ' ';
          _controllerFirstName.text = user.firstName ?? ' ';
          _controllerLastName.text = user.lastName ?? ' ';
          _controllerEmail.text = user.email ?? ' ';
          _controllerCountry.text = user.country ?? ' ';
          _controllerCity.text = user.city ?? ' ';
          _controllerAddress.text = user.address ?? ' ';
          _controllerPhone.text = user.phone ?? ' ';
          _controllerFax.text = user.fax ?? ' ';
          _controllerCellular.text = user.cellular ?? ' ';
          _controllerZipCode.text = user.zipCode ?? ' ';
          //_controllerBirthDay.text = user.birthDay;
          var db = DateTime.parse(user.birthDay ?? DateTime.now().toString());
          var shamsy = Jalali.fromDateTime(db);
          String s = format2(shamsy);
          print('birthDay Formated = ' + s);
          persianDatePicker = PersianDatePicker(
            controller: _controllerBirthDay,
            datetime: s,
          ).init();
        });
      });
    });
  }

  String format2(Date d) {
    final f = d.formatter;

    return '${f.yyyy}/${f.mm}/${f.dd}';
  }

  bool didBirthdayChange = false;
  @override
  Widget build(BuildContext context) {
    Widget loading = Scaffold(
      appBar: AppBar(
        title: Hero(
          tag: 'editProfile',
          child: Text('ویرایش پروفایل'),
        ),
        centerTitle: true,
        backgroundColor: colorAccent,
      ),
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
    Widget loaded = Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.person, color: colorTextWhite, size: 20),
            SizedBox(
              width: 4,
            ),
            Text(
              ((isStillLoading ? ' ' : user.firstName ?? 'ویرایش') +
                  ' ' +
                  (isStillLoading ? ' ' : user.lastName ?? 'پروفایل')),
              style: TextStyle(
                  color: colorTextWhite,
                  fontWeight: FontWeight.bold,
                  fontSize: textFontSizeTitle - 1),
            ),
          ],
        ),
        centerTitle: true,
        backgroundColor: colorAccent,
      ),
      body: SingleChildScrollView(
        child: Container(
          height: 625,
          padding: EdgeInsets.only(top: 8, right: 16, left: 16, bottom: 0),
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  //top card

                  Card(
                    margin: EdgeInsets.all(0),
                    elevation: 3,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(50),
                            bottomRight: Radius.circular(10))),
                    child: Container(
                      padding: EdgeInsets.only(
                          left: 8, right: 8, top: 8, bottom: 12),
                      //top
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          // top right
                          Expanded(
                            flex: 7,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        padding:
                                            EdgeInsets.only(left: 4, top: 4),
                                        child: TextField(
                                          controller: _controllerFirstName,
                                          decoration: InputDecoration(
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    color: Colors.grey[300],
                                                    width: 1.0),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                borderSide: BorderSide(
                                                    color: Colors.blue,
                                                    width: 2.0),
                                              ),
                                              isDense: true,
                                              labelText: 'نام'),
                                          style: TextStyle(
                                            fontSize: textFontSizeTitle - 1,
                                            color: colorTextPrimary,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        padding:
                                            EdgeInsets.only(left: 4, top: 4),
                                        child: TextField(
                                          controller: _controllerLastName,
                                          decoration: InputDecoration(
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    color: Colors.grey[300],
                                                    width: 1.0),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                borderSide: BorderSide(
                                                    color: Colors.blue,
                                                    width: 2.0),
                                              ),
                                              isDense: true,
                                              labelText: 'نام خانوادگی'),
                                          style: TextStyle(
                                            fontSize: textFontSizeTitle - 1,
                                            color: colorTextPrimary,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        padding:
                                            EdgeInsets.only(left: 4, top: 4),
                                        child: TextField(
                                          readOnly: true,
                                          controller: _controllerUsername,
                                          decoration: InputDecoration(
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    color: Colors.grey[300],
                                                    width: 1.0),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                borderSide: BorderSide(
                                                    color: Colors.blue,
                                                    width: 2.0),
                                              ),
                                              isDense: true,
                                              labelText: 'نام کاربری'),
                                          style: TextStyle(
                                            fontSize: textFontSizeTitle - 1,
                                            color: colorTextPrimary,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      child: Container(
                                        padding:
                                            EdgeInsets.only(left: 4, top: 4),
                                        margin:
                                            EdgeInsets.only(bottom: 8, top: 8),
                                        child: TextField(
                                          controller: _controllerPhone,
                                          keyboardType: TextInputType.phone,
                                          decoration: InputDecoration(
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    color: Colors.grey[300],
                                                    width: 1.0),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                borderSide: BorderSide(
                                                    color: Colors.blue,
                                                    width: 2.0),
                                              ),
                                              isDense: true,
                                              labelText: 'تلفن'),
                                          style: TextStyle(
                                            fontSize: textFontSizeTitle - 1,
                                            color: colorTextPrimary,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  //
                                  children: <Widget>[
                                    Expanded(
                                      child: Container(
                                        padding:
                                            EdgeInsets.only(left: 4, top: 4),
                                        child: TextField(
                                          controller: _controllerEmail,
                                          decoration: InputDecoration(
                                              enabledBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(10),
                                                borderSide: BorderSide(
                                                    color: Colors.grey[300],
                                                    width: 1.0),
                                              ),
                                              focusedBorder: OutlineInputBorder(
                                                borderRadius:
                                                    BorderRadius.circular(5),
                                                borderSide: BorderSide(
                                                    color: Colors.blue,
                                                    width: 2.0),
                                              ),
                                              isDense: true,
                                              labelText: 'پست الکترونیک'),
                                          style: TextStyle(
                                            fontSize: textFontSizeTitle - 1,
                                            color: colorTextPrimary,
                                            height: 1,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                          // profile image
                          Expanded(
                            flex: 3,
                            child: Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(35),
                                      topRight: Radius.circular(10))),
                              child: Image(
                                image: AssetImage('images/person.png'),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),

                  Expanded(
                    child: Card(
                      margin: EdgeInsets.all(0),
                      elevation: 3,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(50),
                              topRight: Radius.circular(10))),
                      child: Container(
                        //height: MediaQuery.of(context).size.height,
                        padding: EdgeInsets.only(
                            left: 8, right: 8, top: 8, bottom: 0),
                        child: Column(
                          children: <Widget>[
                            Row(
                              //
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 4, top: 8),
                                    child: TextField(
                                      controller: _controllerAddress,
                                      decoration: InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(10),
                                              topLeft: Radius.circular(60),
                                              topRight:Radius.circular(10),
                                              bottomRight: Radius.circular(10),
                                            ),
                                            borderSide: BorderSide(
                                                color: Colors.grey[300],
                                                width: 1.0),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius: BorderRadius.only(
                                              bottomLeft: Radius.circular(5),
                                              topLeft: Radius.circular(30),
                                              topRight:Radius.circular(5),
                                              bottomRight: Radius.circular(5),
                                            ),
                                            borderSide: BorderSide(
                                                color: Colors.blue, width: 2.0),
                                          ),
                                          isDense: true,
                                          labelText: 'آدرس'),
                                      style: TextStyle(
                                        fontSize: textFontSizeTitle - 1,
                                        color: colorTextPrimary,
                                        height: 1,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              //
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 4, top: 8),
                                    margin: EdgeInsets.only(bottom: 8, top: 8),
                                    child: TextField(
                                      onTap: () {
                                        FocusScope.of(context).requestFocus(
                                            new FocusNode()); // to prevent opening default keyboard
                                        showModalBottomSheet(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return persianDatePicker;
                                            });
                                      },
                                      enableInteractiveSelection: false,
                                      //keyboardType: TextInputType.number,
                                      controller: _controllerBirthDay,
                                      decoration: InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                color: Colors.grey[300],
                                                width: 1.0),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            borderSide: BorderSide(
                                                color: Colors.blue, width: 2.0),
                                          ),
                                          isDense: true,
                                          labelText: 'تاریخ تولد'),
                                      style: TextStyle(
                                        fontSize: textFontSizeTitle - 1,
                                        color: colorTextPrimary,
                                        height: 1,
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 4, top: 8),
                                    margin: EdgeInsets.only(bottom: 8, top: 8),
                                    child: TextField(
                                      controller: _controllerZipCode,
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              signed: true),
                                      decoration: InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                color: Colors.grey[300],
                                                width: 1.0),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            borderSide: BorderSide(
                                                color: Colors.blue, width: 2.0),
                                          ),
                                          isDense: true,
                                          labelText: 'کد پستی'),
                                      style: TextStyle(
                                        fontSize: textFontSizeTitle - 1,
                                        color: colorTextPrimary,
                                        height: 1,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              //
                              children: <Widget>[
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 4, top: 8),
                                    child: TextField(
                                      controller: _controllerCellular,
                                      keyboardType: TextInputType.phone,
                                      decoration: InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                color: Colors.grey[300],
                                                width: 1.0),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            borderSide: BorderSide(
                                                color: Colors.blue, width: 2.0),
                                          ),
                                          isDense: true,
                                          labelText: 'تلفن همراه'),
                                      style: TextStyle(
                                        fontSize: textFontSizeTitle - 1,
                                        color: colorTextPrimary,
                                        height: 1,
                                      ),
                                    ),
                                  ),
                                ),
                                Expanded(
                                  child: Container(
                                    padding: EdgeInsets.only(left: 4, top: 8),
                                    child: TextField(
                                      controller: _controllerFax,
                                      keyboardType:
                                          TextInputType.numberWithOptions(
                                              signed: true),
                                      decoration: InputDecoration(
                                          enabledBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            borderSide: BorderSide(
                                                color: Colors.grey[300],
                                                width: 1.0),
                                          ),
                                          focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5),
                                            borderSide: BorderSide(
                                                color: Colors.blue, width: 2.0),
                                          ),
                                          isDense: true,
                                          labelText: 'فکس'),
                                      style: TextStyle(
                                        fontSize: textFontSizeTitle - 1,
                                        color: colorTextPrimary,
                                        height: 1,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Container(
                              padding: EdgeInsets.only(left: 4, top: 8),
                              margin: EdgeInsets.only(bottom: 8, top: 8),
                              child: TextField(
                                obscureText: true,
                                controller: _controllerPassword,
                                decoration: InputDecoration(
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(10),
                                      borderSide: BorderSide(
                                          color: Colors.grey[300], width: 1.0),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5),
                                      borderSide: BorderSide(
                                          color: Colors.blue, width: 2.0),
                                    ),
                                    isDense: true,
                                    labelText: 'رمز عبور'),
                                style: TextStyle(
                                  fontSize: textFontSizeTitle - 1,
                                  color: colorTextPrimary,
                                  height: 1,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            RaisedButton.icon(
                              onPressed: () {
                                submitUserEdit();
                              },
                              color: colorPrimary,
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10)),
                              textColor: colorTextWhite,
                              label: Padding(
                                padding: const EdgeInsets.only(
                                    top: 12, left: 16, bottom: 12),
                                child: Text('ثبت تغییرات'),
                              ),
                              icon: Icon(Icons.check, color: colorTextWhite),
                            ),
                            //Expanded(child: Container(),)
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Container(
                child: Dash(
                    length: 330,
                    dashColor: Colors.grey.shade100,
                    dashThickness: 3),
                margin: EdgeInsets.only(top: 190, right: 8),
              ),
            ],
          ),
        ),
      ),
    );

    return isStillLoading ? loading : loaded;
  }

  Future<http.Response> _putEditUser(
      String userInfo, String uid, String accessToken) async {
    // set up POST request arguments
    String url = apiServer + 'user/' + uid;
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken',
      "Content-type": "application/json"
    };
    String json = userInfo;
    // make POST request

    try {
      http.Response response =
          await http.put(url, headers: requestHeaders, body: json);
      return response;
    } catch (e) {
      print('failed');
      throw Exception('failed');
    }
  }

  void submitUserEdit() {
    getAccessToken().then((accessToken) {
      String userInfo = '{ ' +
          '"usr_address": "' +
          _controllerAddress.text +
          '",' +
          '"usr_cellular": "' +
          _controllerCellular.text +
          '",' +
          '"usr_cnf_pass": "' +
          _controllerPassword.text +
          '",' +
          '"usr_email": "' +
          _controllerEmail.text +
          '",' +
          '"usr_fax": "' +
          _controllerFax.text +
          '",' +
          '"usr_firstname": "' +
          _controllerFirstName.text +
          '",' +
          '"usr_lastname": "' +
          _controllerLastName.text +
          '",' +
          '"usr_new_pass": "' +
          _controllerPassword.text +
          '",' +
          '"usr_phone": "' +
          _controllerPhone.text +
          '",' +
          '"usr_uid": "' +
          user.uID +
          '",' +
          '"usr_username": "' +
          _controllerUsername.text +
          '",' +
          '"usr_zip_code": "' +
          _controllerZipCode.text +
          '"' +
          '}';
      _putEditUser(userInfo, user.uID, accessToken).then((response) {
        if (response.statusCode == 200) {
          showInSnackBar('تغییرات اعمال شد');
          Future.delayed(const Duration(milliseconds: 2000), () {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (BuildContext context) => UserPage()));
          });
        } else if (response.statusCode == 400) {
          showInSnackBar('لطفا اطلاعات را کامل وارد کنید');
        } else {
          showInSnackBar('خطا در اتصال به اینترنت');
        }
      });
    });
  }

  void showInSnackBar(String value) {
    FocusScope.of(context).requestFocus(new FocusNode());
    _scaffoldKey.currentState?.removeCurrentSnackBar();
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(
        value,
        textAlign: TextAlign.center,
        style: TextStyle(
            color: colorTextWhite,
            fontSize: textFontSizeTitle - 1,
            fontFamily: "Sans"),
      ),
      backgroundColor: colorSecond,
      duration: Duration(seconds: 3),
    ));
  }
}
