import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import 'GlobalData.dart';

class UserIframesPage extends StatefulWidget {
  final String url;
  final String appUID;
  final int type;
  final String delIndex;
  final String sessionId;

  UserIframesPage({
    this.url,
    this.appUID,
    this.type,
    this.delIndex,
    this.sessionId,
  });
  @override
  _UserIframesPageState createState() => _UserIframesPageState();
}

class _UserIframesPageState extends State<UserIframesPage> {
  String baseUrl;
  String link;
  String sessionId;
  bool isLoaded = false;
  @override
  dispose() {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      //DeviceOrientation.landscapeLeft,
    ]);
    //print('url = ' + widget.url);
    baseUrl = iframeServer + '';
    getAccessToken().then((accessToken) {
      _fetchSessionID(accessToken).then((sid) {
        setState(() {
          sessionId = sid;
          link = _setUrl();
          print('link = ' + link);
          isLoaded = true;
        });
      });
    });

    // http://ls.arian.co.ir:8081/sysarian/fa/modern/cases/cases_Open?APP_UID=6696583185d9c686a70f0b9045007826&DEL_INDEX=3$sid=3119689435da43a04b32366005199005
  }

  Future<String> getAccessToken() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString('access_token') == null
        ? ' '
        : prefs.getString('access_token');
    return accessToken;
  }

  Future<String> _fetchSessionID(String accessToken) async {
    Map<String, String> requestHeaders = {
      'Authorization': 'Bearer $accessToken'
    };
    String url =
        apiServer + 'extrarest/session-id';
    var response = await http.get(url, headers: requestHeaders);

    if (response.statusCode == 200) {
      String sessionId;

      var items = jsonDecode(response.body);

      //print('response.body = ' + response.body.toString());
      sessionId = items.toString();
      print('sessionId = ' + sessionId);
      return sessionId;
    } else {
      print('Exception');
      throw Exception('Failed to load internet');
    }
  }

  String _setUrl() {
    String url = baseUrl;
    if (widget.type == 0) {
      url =
          iframeServer + 'cases/cases_Open?APP_UID=' +
              widget.appUID +
              '&DEL_INDEX=' +
              widget.delIndex +
              '&sid=' +
              sessionId;
    } else {}
    return url;
  }

  @override
  Widget build(BuildContext context) {
    return isLoaded
        ? WebviewScaffold(
            url: link,
            appBar: new AppBar(
              title: new Text("آرین نوین رایانه"),
              centerTitle: true,
              backgroundColor: colorAccent,
            ),
            displayZoomControls: true,
          )
        : Scaffold(
            body: Center(child: CircularProgressIndicator()),
          );
  }
}
