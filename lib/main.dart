import 'package:my/MainPage.dart';
import 'package:flutter/material.dart';
import 'Login.dart';
import 'SplashScreen.dart';
import 'User/UserChangePassPage.dart';
import 'User/UserPage.dart';
import 'UserIframesPage.dart';

void main() {
  runApp(MaterialApp(
    color: Colors.white,
    routes: <String, WidgetBuilder>{
      '/LoginPage': (BuildContext context) => Directionality(
            textDirection: TextDirection.ltr,
            child: new Builder(
              builder: (BuildContext context) {
                return new MediaQuery(
                  data: MediaQuery.of(context).copyWith(
                    textScaleFactor: 1.0,
                  ),
                  child: LoginPage(),
                );
              },
            ),
          ),
      '/MainPage': (BuildContext context) => MainPage(),
      '/UserPage': (BuildContext context) => UserPage(),
      '/UserIframesPage': (BuildContext context) => UserIframesPage(),
      '/UserChangePassPage': (BuildContext context) => UserChangePassPage(),
    },
    debugShowCheckedModeBanner: false,
    builder: (BuildContext context, Widget child) {
      return new Directionality(
        textDirection: TextDirection.rtl,
        child: new Builder(
          builder: (BuildContext context) {
            return new MediaQuery(
              data: MediaQuery.of(context).copyWith(
                textScaleFactor: 1.0,
              ),
              child: child,
            );
          },
        ),
      );
    },
    theme: ThemeData(fontFamily: 'Sans'),
    home: Main(),
  ));
}

class Main extends StatefulWidget {
  @override
  _MainState createState() => _MainState();
}

class _MainState extends State<Main> {
  bool test = false;
  @override
  void initState() {
    super.initState();
    
  }

  @override
  Widget build(BuildContext context) {
    return SplashScreen();
    //return MainPage();
  }

  
}
