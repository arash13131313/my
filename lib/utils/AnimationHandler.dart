import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';

class AnimationHandler{

  Widget popUp(Widget child,Curve curve){
    final tween = MultiTrackTween([
      Track("scale").add(
          Duration(milliseconds: 350), Tween(begin: 0.0, end: 1.0),
          curve: curve),
      Track("opacity")
          .add(Duration(milliseconds: 200), Tween(begin: 0.5, end: 1.0))
    ]);

    return ControlledAnimation(

      delay: Duration(milliseconds: (700).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builderWithChild: (context, child, animation) => Opacity(
        opacity: animation["opacity"],
        child: Transform.scale(scale: animation["scale"], child: child),
      ),
    );
  }
  Widget translateFromLeft(Widget child,Curve curve,double delay){
    final tween = MultiTrackTween([
      Track("translateX").add(
          Duration(milliseconds: 300), Tween(begin: 75.0, end: 0.0),
          curve: curve),
      Track("opacity")
          .add(Duration(milliseconds: 200), Tween(begin: 0.5, end: 1.0))
    ]);

    return ControlledAnimation(

      delay: Duration(milliseconds: (300 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builderWithChild: (context, child, animation) => Opacity(
        opacity: animation["opacity"],
        child: Transform.translate(offset: Offset(-animation["translateX"], 0.0), child: child),
      ),
    );
  }
  Widget translateToLeft(Widget child,Curve curve,double delay){
    final tween = MultiTrackTween([
      Track("translateX").add(
          Duration(milliseconds: 500), Tween(begin: 0.0, end: 130.0),
          curve: curve),
      Track("opacity")
          .add(Duration(milliseconds: 500), Tween(begin: 1.0, end: 0.5))
    ]);

    return ControlledAnimation(

      delay: Duration(milliseconds: (300 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builderWithChild: (context, child, animation) => Opacity(
        opacity: animation["opacity"],
        child: Transform.translate(offset: Offset(-animation["translateX"], 0.0), child: child),
      ),
    );
  }
  Widget translateFromRight(Widget child,Curve curve,double delay){
    final tween = MultiTrackTween([
      Track("translateX").add(
          Duration(milliseconds: 300), Tween(begin: -75.0, end: 0.0),
          curve: curve),
      Track("opacity")
          .add(Duration(milliseconds: 200), Tween(begin: 0.5, end: 1.0))
    ]);

    return ControlledAnimation(

      delay: Duration(milliseconds: (300 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builderWithChild: (context, child, animation) => Opacity(
        opacity: animation["opacity"],
        child: Transform.translate(offset: Offset(-animation["translateX"], 0.0), child: child),
      ),
    );
  }
}